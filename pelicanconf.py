#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Ekaitz Zárraga'
SITENAME = u'Ekaitz\'s tech blog'
SITESUBTITLE = u'I make stuff at ElenQ Technology and I talk about it'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'www'
STATIC_PATHS = ('static',)
PAGE_PATHS = ('pages',)
#ARTICLE_PATHS = ('',)

TIMEZONE = 'Europe/Athens'

DEFAULT_LANG = u'en'
LOCALE = ('en_US.UTF8',)


# Category folders
USE_FOLDER_AS_CATEGORY = False

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None


# Blogroll
LINKS = (('ElenQ Technology', 'https://elenq.tech/'),)

# Social widget
SOCIAL = ()

DEFAULT_PAGINATION = 20

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# Tipography improvements
TYPOGRIFY = True

# Theme
THEME = 'themes/elenq'

# Menu
DISPLAY_CATEGORIES_ON_MENU = False
DISPLAY_PAGES_ON_MENU = True
MENUITEMS = [("Home" , "/index.html"), ("Series", "/tags.html")]

# Tags
# Doesn't work with links correctly :S
#TAGS_SAVE_AS = 'series.html'
#TAG_SAVE_AS  = 'series/{slug}.html'

# Markdown extras
MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.attr_list': {},
        'markdown.extensions.fenced_code': {},
        'markdown.extensions.footnotes': {},
        'markdown.extensions.tables': {},
        'markdown.extensions.codehilite': {
            'css_class': 'highlight',
            'use_pygments': False,
            # Pygments breaks the semantics of the HTML inserting `span`
            # elements in a `pre` instead of a `pre` with `code` inside.
            # The theme uses optional JS for this (prismjs.com), this way
            # semantics are mantained.
            # See also: https://github.com/getpelican/pelican/issues/2569
        },
        # optionally, more extensions,
        # e.g. markdown.extensions.meta
    },
    'output_format': 'html5',
}
