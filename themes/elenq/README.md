# ElenQ Technolog pelican theme


## Weird details

- This theme uses `tags` as `series` of articles. The idea is to keep articles
  with multiple parts ordered in the same tag. Tag page will order all the
  articles from older to newer like they were chapters of a book.
