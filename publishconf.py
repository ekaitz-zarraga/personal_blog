#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

SITEURL = 'https://ekaitz.elenq.tech'
RELATIVE_URLS = True

# Translations
ARTICLE_TRANSLATION_ID = 'slug'
PAGE_TRANSLATION_ID = 'slug'

# Feeds
FEED_DOMAIN = SITEURL
FEED_ALL_ATOM = 'feeds/all.atom.xml'
TRANSLATION_FEED_ATOM = 'feeds/all-{lang}.atom.xml'
#CATEGORY_FEED_ATOM = 'feeds/{slug}.atom.xml'
#AUTHOR_FEED_ATOM = 'feeds/{slug}.atom.xml'

DELETE_OUTPUT_DIRECTORY = True

MASTODON_PROFILE = 'https://mastodon.social/@ekaitz_zarraga'
MASTODON_HANDLE = '@ekaitz_zarraga@mastodon.social'
