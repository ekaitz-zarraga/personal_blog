Title: Screencasts: discussing with ffmpeg
Date: 2020-01-11
Category:
Tags:
Slug: ffmpeg-screencast
Lang: en
Summary: Screencasts in `ffmpeg`, having some fun solving issues thanks to
    other people

When you battle using your arguments that's called a *discussion*... That's
exactly what I've been doing for a couple of days with `ffmpeg`: I've been
using arguments trying to reach an understanding.

I wanted to record my screen, and a couple of cameras, for reasons you'll know
about later, and I didn't want to play around with new GUI programs and
configuration so I decided to go for a simple `ffmpeg` setup.

You probably had played with `ffmpeg` in the past. It has tons of different
input arguments and options. It's crazy.

Most of my previous times using it were just video conversions and it's as
simple as choosing the right extensions for files, but when it comes to video
and audio recording it gets complicated. I have no idea about video and audio
encodings and I don't really have the time to dig in such an exciting topic.
I searched on the Internet for examples and I found some: cool.

I played around with multiple inputs and outputs, I changed arguments I can't
even remember now and it kinda worked until I decided to record my voice at the
same time. **Delay**.

What to do then?

Just go to the internet an keep searching.

I found a project called `vokoscreen` that now is archived because it's
migrating from `ffmpeg` to `gstreamer` (I also struggled with gstreamer in the
past but that's another story) but it worked fine. It was in the repos of my
distro, it only asked me to install one dependency, a couple of megs only...
Great!

I tried to make a screencast and the audio worked like a charm. I went for the
code, read it and [realized the arguments it uses to call `ffmpeg` are easy to
find][vokoscreen].

Even better, in the program itself there's a button to show a log of what it
does and it dumps the exact call it does.

With that and some extra things I learned from the investigation in the deep
abyss of the Internet, boom! There it goes:

    ::bash
    ffmpeg
        -y -f x11grab -draw_mouse 1 -framerate 25 -video_size 1920x1080 -i :0+0,0 \
        -f alsa -ac 2 -i default \
        -pix_fmt yuv420p -c:v libx264 -preset veryfast \
        -c:a libmp3lame -q:v 1 -s 1920x1080 -f matroska \
        output.mkv

Today in half an hour I solved the thing I've been struggling with a couple of
days. But I think I wouldn't be able to solve it if I didn't struggle with it
last days... I don't know.

The good thing is I learned a couple of things from this I'll write down here
to avoid forgetting them:

### Multiple inputs

Like the command in the example, `ffmpeg` can get multiple inputs. In the case
of the example, they are `x11grab` (my screen) and `alsa`'s default input (the
microphone). More inputs can be combined, like music playing in the background
or whatever you want.

### Multiple outputs

There's also the chance to put multiple outputs there just like the multiple
input thing does but in the output part of the command[^1].

#### Pipe

You can even pipe the command to a different one, like:

```
ffmpeg [...] - | ffplay -i -
```

In this case you can use one of the outputs to record to a file and another
one to `ffplay` which plays the video in screen.

This is useful if you want to record from a webcam and you want to see what you
are recording.


### Closing note

So yeah, I was an ignorant about `ffmpeg` and I still am.

But at least I learned a couple of the arguments and learned how to deal with
all my cameras and screens at the same time.

Good enough.

I mean, it works, right? And that's the most important thing[^2].

[^1]: Yes, it's hard to know where's the input and where's the output.
[^2]: It's not. The most important thing is to be happy, do what you like,
  enjoy your life and feel appreciated and valued. If your software works it's
  like... Uugh... Congratulations I guess?

[vokoscreen]: https://github.com/vkohaupt/vokoscreen/blob/b5865a85561baa46e627c09cf77efb7369516327/screencast.cpp#L2718
