Title: [ES] Génesis
Date: 2018-04-15
Tags:
Slug: Genesis
Lang: es
Translation: true

Como primer post en este blog *oficial-pero-no-muy-oficial* sólo quiero
presentarme y presentar *ElenQ Technology*.

Me llamo Ekaitz Zárraga y nací en 1991. Suelo describir mi trabajo como
ingeniero de I+D pero en realidad estudié Ingeniería de Telecomunicaciones y
*sólo* trabajo en ese área. Mayormente me centro en actividades relacionadas
con los ordenadores como, por ejemplo, programar pero también puedo hacer
electrónica y otras cosas. Esa sería mi presentación formal. En la informal
diría que soy una persona bastante curiosa, lo que me ha hecho investigar y
profundizar en otras disciplinas como el arte en sus diferentes formas. Este
último punto explica mucho de lo que vendrá después en este texto. Y eso es
todo en lo que a mí respecta, ya escribiré un currículum vitae informal en el
futuro.

*ElenQ Technology* es un nombre, una forma de llamar a como soy y a los
intereses que tengo. Además, también es un proyecto de I+D que estoy
desarrollando. Es una empresa distinta a las demás, en la que pretendo generar
conciencia acerca de la tecnología ética mediante el ejemplo, demostrando que
las compañías de tecnología ética pueden ser rentables. No es sólo mi trabajo
en el que hago ingeniería, también es una *performance* artística. Es como una
obra de arte.

> *ElenQ Technology* es un proyecto artístico que te dice que otro modelo es
> posible. Te recuerda que tienes elección y que no tienes que trabajar en una
> corporación y seguir sus reglas.

*ElenQ Technology* es, simplemente, el resultado de todas las cosas que he
sentido trabajando para otras compañías y el resultado de un análisis profundo
del estado de la tecnología en mi contexto cercano que, creo, puede ser
extrapolado al resto de lugares con una precisión aceptable.

Para contextualizar, las grandes empresas del mundo IT en mi zona cercana
tienen un modelo de negocio similar, basado en el *body shopping* (muchas de
ellas haciendo cesiones ilegales en subcontratas). Pagan unos salarios
bajísimos y las condiciones laborales son lamentables. El resto de sectores
tampoco están mucho mejor, pero el caso de las empresas del mundo IT es
escalofriante.

Los trabajos rara vez son de 8 horas diarias, las jornadas se están alargando
cada vez más y, en muchos, es normal trabajar 10 horas al día. La famosa
*Crisis Económica™* mezclada con una profunda corrupción ha sido el caldo de
cultivo perfecto para que las grandes empresas se aprovechen de los
trabajadores.

Dicho esto, es muy fácil entender cómo funciona el mundo de la tecnología por
aquí. Grandes empresas ganando insultantes cantidades de dinero mientras que no
respetan a sus trabajadores o clientes, soluciones tecnológicas privativas para
atar a los clientes e impedirles ser independientes, etc. Todo para mantener su
modelo de negocio podrido y corrupto hasta la médula.

Ese es el estado de las empresas de tecnología en mi entorno, el de las
grandes. Seguro que puede extrapolarse a otros lugares porque muchas de ellas
operan también en el extranjero.

En mi caso tuve la suerte de acabar en una empresa que parecía un lugar mejor.
Las condiciones eran ligeramente mejores que las que he descrito, o al menos
así lo creía yo. Era un trabajo de Ingeniero de I+D en una empresa no demasiado
grande. Trabajaba en un departamento aislado de menos de 10 personas.  Hacíamos
los juguetes nuevos de la empresa. Era divertido.

Después de algún tiempo allí me di cuenta de cómo funcionaba. No era tan
diferente al resto. Hubo muchas cosas que no quiero compartir aquí pero empecé
a sentirme bastante mal y mi situación personal tampoco ayudó mucho. Siempre he
sido una persona curiosa a la que le gusta aprender cosas nuevas y ese trabajo
dejó de aportarme eso como lo hacía al principio. Empecé a necesitar llenar ese
hueco trabajando en mis proyectos personales en el poco tiempo que me quedaba
al día. La suma de un entorno de trabajo aburrido y deprimente más los
problemas organizativos que teníamos era difícil de gestionar.

Sumergido en ese entorno deprimente, la empresa, con intención de salir a bolsa
próximamente, quiso exprimir al máximo sus recursos y plantear nuevos negocios.
Nuestro departamento, como encargado del I+D de la empresa, era el responsable
de plantear las nuevas *pruebas de concepto*. Nos pidieron que analizásemos los
datos de la compañía. Literalmente, nos pidieron que siguiésemos a la gente,
que los localizásemos. No les importaba que fuesen nuestros clientes o no.
Querían que localizásemos a *todos*.

Eso fue la gota que colmó el vaso. Tenía que dejarlo porque eso superaba con
creces el límite de mi ética personal. No me gustan esas prácticas y no podía
ser parte de eso.

Llevaba tiempo pensando en la forma en la que hacemos tecnología y siempre me
había apetecido probarlo por mi cuenta. Eso me dio el valor que me faltaba
para hacerlo.

¿Por qué no simplemente cambiar de trabajo?

Creo que era el momento para intentarlo. Como entusiasta del software y
hardware libre, siempre me ha interesado definir lo que es la tecnología ética
y llevaba tiempo con ganas de aplicarlo en mi campo: el I+D. Además, teniendo
en cuenta el estado de las empresas para las que podía trabajar, decidí cambiar
las cosas de raíz. Decidí intentar un modelo distinto. ¿Tenía alguna otra
alternativa en realidad?

¿Es una alternativa real trabajar en un entorno deprimente que hace del mundo
un lugar peor? ¿Seguro?

Piensa en ello.

*ElenQ Technology* rompe entonces con ese modelo de negocio y hace tecnología
ética de una forma ética. Eso es bueno para mí porque me permite trabajar en
los campos que me gustan y hacer del mundo un lugar mejor lo que, egoístamente,
mejorará el futuro que le deje a mis hijos, si algún día los tengo. Al mismo
tiempo esto es bueno para los clientes de *ElenQ Technology* porque los
proyectos se gestionan de forma que *ellos* son los dueños de la tecnología que
se crea. Para esto último se siguen los principios del Software y el Hardware
Libre y el [Manifiesto del Diseño Ético](https://2017.ind.ie/ethical-design/)
junto con algunas ideas adicionales más específicas del campo al que me dedico
(puedes leer más [aquí][1]).

Quiero cambiar el mundo. ¿Tú no?

El problema es que yo no puedo hacerlo solo así que *ElenQ Technology* es una
forma de hacer que otros tomen la misma decisión que yo tomé (o fui forzado a
tomar) y ese es su objetivo principal.

Cambiemos el mundo juntos.

### Sobre este blog

Ya te has dado cuenta que este blog no es el blog oficial de *ElenQ
Technology*. Esto es *mi* blog *oficial-pero-no-muy-oficial* como parte (o
"persona al frente", pero decirlo así no me gusta) de *ElenQ Technology*.

Aquí escribiré sobre *ElenQ Technology*, sobre su filosofía, objetivos, logros
y ese tipo de temas *oficiales* que me parezcan relevantes pero sobre todo
tengo la intención de escribir sobre las **cosas que hacemos**. Eso es lo que
más me interesa.

Este sitio será un lugar muy técnico en el que trataré de explicar conceptos
avanzados de forma sencilla para que aprendáis conmigo. Quiero compartir lo que
haga con vosotros.

### Sobre los idiomas

Este blog se escribe en inglés, pero algunas de las entradas (como esta misma)
podrán traducirse a otros idiomas. Como el blog soporta traducciones, prefiero
mantener las opción activa para, si es necesario, añadir traducciones
proporcionadas por la comunidad o hechas por mí mismo, como en este caso.

[1]: https://elenq.tech/es/about.html#ethical-innovation
