Title: Bye Protonmail
Date: 2023-11-21
Category:
Tags:
Slug: bye-protonmail
Lang: en
Summary:
    I left Protonmail. Here is why. I still like them to some degree though.


The other day in the fediverse a friend of mine asked me about Protonmail.
I explained a little bit my feelings and Protonmail jumped in, making me
finally explain further. I think the conversation is interesting enough to
share here[^deleted].

[^deleted]: My posts in Mastodon are also automatically deleted so if you try
    to read it later in there you might not find it. I'm copying it here as a
    reference.

<section class="masto-thread">

<article class="masto-toot">
<a href="https://mastodon.social/@ekaitz_zarraga"> Ekaitz Zárraga 👹 </a>
<p>I really like <code>@protonmail</code> but they are always getting in the way
with their non-standard things and their bridge which is FULL of dependencies
and it's impossible to package for some systems.</p>

<p>They are pushing me away from them too hard...</p>

<p>I won't be surprised if finally push me away from their service in the mid
term... after many years of trusting them for my business and personal email...
It's a real shame.</p>
</article>

<article class="masto-toot">
<a href="https://mastodon.social/@protonmail"> Proton Mail</a>

<p><code>@ekaitz_zarraga</code> Can you let us know what kind of dependencies you're
referring to?</p>
</article>


<article class="masto-toot">
<a href="https://mastodon.social/@ekaitz_zarraga"> Ekaitz Zárraga 👹 </a>

<p><code>@protonmail</code> the Proton bridge has many dependencies:<br>
<a href="https://github.com/ProtonMail/proton-bridge/blob/master/go.mod">
https://github.com/ProtonMail/proton-bridge/blob/master/go.mod</a></p>

<p>Packaging all of them for a distribution is a huge effort. I don't think you
are really aware of the level of work it requires. Also, you have a .deb and a
.rpm package, which are precompiled... forcing your users to trust those.</p>

<p>My distro and my work are focused on reproducible builds and
bootstrappability... some serious concerns you don't take in account.</p>
</article>

<article class="masto-toot">
<a href="https://mastodon.social/@ekaitz_zarraga"> Ekaitz Zárraga 👹 </a>

<p><code>@protonmail</code> Also, don't get me wrong. I love protonmail and its
ideas but I think you are too focused on "normal" users and breaking other
people's setups without giving much in exchange. I feel like a second class
user in protonmail, as my distro doesn't support .deb or .rpm packages... and I
need to use plain text email pretty often (which you don't really support in
the web either).</p>
</article>

<article class="masto-toot">
<a href="https://mastodon.social/@ekaitz_zarraga"> Ekaitz Zárraga 👹 </a>

<p><code>@protonmail</code> I love protonmail, and I'd love to fix these issues, I
would even make a reproducible bridge for you if you ask me to. But I don't
have the energy to do it by myself. It's simply not possible to package.</p>

<p>So, here we are. As much as I'd like to continue to work with you and support
you, I don't feel I can do it anymore</p>
</article>
</section>

<style>
.masto-toot {
    border: 2px solid var(--border-color);
    border-radius: 10px;
    margin: 1rem;
    padding: 1rem;
}
</style>

Not long later I simply moved my email out of protonmail, to a different
platform. An Europe based email provider that provides an interaction based on
standards. Standards I can use with **any** setup, in **any** machine.

I wouldn't care to have a non-standard solution if the Protonmail Bridge
application worked, but they only provide `.deb` and `.rpm` packages. I can't
package the app, because it has too many dependencies to be done in an
acceptable amount of time.

Also, the Web client is getting more and more complex. My anti-tracking plugins
(like jShelter) tell me they are fingerprinting me when I reach the login
screen. Why? Who knows. I contacted them and told them about this and, of
course, I didn't talk with a technical person, because you are not supposed to
do that, so I don't think my words reached anyone that could understand them,
or consider them.

Maybe it's me who changed. I don't need the default PGP configuration anymore
because I can configure it myself, I realized I am more in the need of being
able to easily `git send-email` than using a beautiful Web UI that tracks me or
uses modern JS features. I use a weird distro now, which shouldn't be a problem
but it happens to be, and I realized having too many dependencies in the code
is often a problem in several dimensions.

So, something that happens too often in my life happened again: being a
technical user has been punished again in favour of the concept of *dumb
users*. The funny thing of all this is I don't think *dumb users* exist. We
should discuss that another time.

They are a company, they want to grow, so they must try to sell for the
*baseline* user. The minimal amount of knowledge a person can have. Selling a
product for "expert" users is lost money, there are not that many "experts" in
this world after all. So it's easier to add layers and layers of complexity to
your software in order to provide a *dumb proof* interface, instead of
educating your userbase, or letting the educated ones to customize their stuff.
Don't get me wrong, it makes perfect sense, and Protonmail's mission is to
provide default encryption to the largest proportion of email users possible so
the decision fits their mission[^good].

The default encryption and the "easy" PGP key setup Protonmail offers is really
cool for users that don't require more level of customization. I still like the
goals of the company but I could've used a simpler way to customize my
experience: maybe a simpler bridge? Maybe something else.. I don't know.

In the end, they pushed me away from their service.

So long, Protonmail. It's been a good time together.

[^good]: Regardless of anything I said here, they are making many people
    encrypt their email, one way or another, and I'll continue to do so. That
    is valuable.
