Title: ElenQ Publishing
Date: 2020-02-18
Category: ElenQ Publishing
Tags:
Slug: elenq-publishing
Lang: en
Summary:
    ElenQ Publishing is a technical book publishing project that aims to open
    the door of technical knowledge to everyone.


Hi all,

This 18th of February of 2020 the crowdfunding campaign of ElenQ Publishing
started and I'd like to talk a bit about it.

<http://en.goteo.org/project/elenq-publishing>


## The platform

First of all, I want to talk you about the crowdfunding platform:
<http://goteo.org>.

Goteo is platform for social crowdfunding that aims to support projects with a
social goal. The software it runs is Free Software licensed under the Affero
GPL license, meaning if you want to make your own crowdfunding platform you can
use the code that Goteo shares, as long as you provide the <del>users</del>
people the source code that is running in your platform (server and client).

Goteo Foundation, the maintainers of the code and the people behind goteo.org,
fund themselves with the 5% of the crowdfunded money from the campaigns, a fair
price for their services. They also receive some help from different government
entities like Barcelona's local government or Spanish Education, Culture and
Sports Ministry because of their social impact.

At least in Spain, people that takes part in the campaigns run in goteo.org
have the chance to declare they donated money for social goals and get some
money back in their tax returns.

For those who want to make a campaign, Goteo reviews and gives feedback, they
make the campaign management easier and they are really focused on being
multilingual. They support translations for the campaigns and the UI is in many
languages, including local all Spanish regional languages and some extra
languages more.

This platform is perfectly aligned with the philosophy of ElenQ Technology.

## The story

I don't talk about it enough, but I've been teaching informatics related topics
since I started with ElenQ Technology. In these 3 years I gave many courses:
introductory python, advanced python, data analysis, web scraping, bitcoin
and blockhain[^blockchain], introductory clojure... And some more I can't
remember at the moment. All of those were done in different contexts, from
courses for young unemployed people to courses for engineers in research
centers. Also, it looks I'm going to keep teaching, because I like it and
the students say I'm good at it.

But this is not good enough. It helps me to make a living but it's not enough.
I want to make my best to correct many of the issues I found in this 3 year
travel.

I realized I have some tested course structure and materials I want to share.

I realized many people's English level is not good enough for learning by
technology by themselves. They needed someone like me to serve as a bridge.
They are isolated from knowledge because the place they come from and the
culture they have.

I realized all the technical publications I was reading were written from the
same perspective of technology. That made sense, because every author authors
came from the same context. I would like to have more diverse people writing
about technology and the only way to do it is to make technology more
accessible.

I realized that, in my local context, access to knowledge is broken in many
ways that looks nobody is willing to change:

- In my area, government backed courses only focus in groups of people that are
  likely to get a job soon anyway. This way they can say they got the job
  thanks to the money the government invested on the courses and win elections
  with that. Young people that finished university this year are likely to get
  a job in the next year. This doesn't mean they don't need the
  course[^course], but the course itself won't really affect their
  employability. What about people with **real** employability
  problems[^employability-problems]?

- Some people have individual problems that don't fit in the goals of social
  campaigns run by the government or other entities because they focus on large
  groups of society with similar problems and they don't focus the individual.
  It makes sense because they can help more efficiently that way, but the net
  has some holes we should repair.

- There is no structural support for people who just want to learn new things
  with no further intention. University is deriving. Now it's just a place
  where you get a paper that helps you get a job, but it's not fulfilling the
  goals of knowledge it should. It's not a place where you find knowledge
  anymore. Many people don't want to think or learn, but some do and we are
  preventing them from doing it.

- In other places the problem of education is even worse and they don't have
  resources (or will[^will]) to solve it. Individuals shouldn't suffer from
  that. It's our responsibility to help everyone have all the chances to
  develop themselves as much as they want, regardless of their context.

In general, all the points are summarized in one: Knowledge should be free
(libre). If it's not free it's not knowledge, is just something that makes you
more powerful than others: it's injustice.

I realized many of these things could be solved with a good repository of
knowledge in different languages, and, as I'm teaching stuff and I like to
write, I considered interesting to work a little bit more on the notes I give
my students and make them look like a book.

With [a little bit of
effort](https://ekaitz.elenq.tech/templates-released.html), I can make a book
that can be published on the web, on a physical book and on a easy-to-print PDF
that anyone can print and copy in a local print shop. I can make it arrive any
place in this world.

Not only that. Some people designed a license that lets others create new
contents on top of what I did and force them to share what they did with the
same license: Creative Commons.

So, with some effort and some funding (and a smile in my face) I can create a
publishing project where I gather all the knowledge that my job makes me deal
with and I can share it in a way that is ethical and respects everyone's
circumstances.

This is something I want to try. It's something I *need* to try.

[^blockchain]: don't judge me too fast: the course was a technical explanation
  about every detail of how does bitcoin work. I wanted students to learn the
  cryptography behind and all the good design ideas bitcoin has while I tried
  to make them be critical about the blockchain technology during the
  blockchain boom.

[^course]: They need it, more if its a course like mine where I talk them about
  working with ethics and being independent. :D

[^employability-problems]: Say: women, unemployed people in their 50s dropped
  out of their jobs because of the
  [de-industrialization](https://en.wikipedia.org/wiki/Deindustrialization),
  immigrants, people with disabilities, people that just get out of jail...

[^will]: USA, I'm looking at you.

## The campaign

That's why I'm trying it.

This campaign is the first attempt to make this happen. If it's successful, it
will make me spend some of my time giving love to the contents I want to
release and let me talk with people who have knowledge in areas I don't and
help them publish it.

The campaign has physical books as a goal but they are just a vehicle to be
able to publish them in a way that is easy to share. The physical objects are
just a way to get funds.

The main goal is not just to make books: it's the creation of an infrastructure
to share knowledge that I can use for the things I research but it can be used
also for things other people researches. All the content is going to be
published in raw in a repository that anyone could audit, review, improve or
create a new project based on it.

Once the infrastructure is ready, publishing new books should be a piece of
cake. This first project is going to teach us how to make the paperwork for the
ISBN and the book registration and is going to give us time to create the
website where the content is going to be stored. Once all those points are
ready, the rest of it is "just" write and publish.

The campaign's goals are separated in two levels: the minimum and the optimum.

The minimum is the publishing of the books in Spanish, my mother tongue
language, and covers all the infrastructure costs of that. This way Spanish
speaking people will have at least some technical books in their language.

The optimum ensures the publishing in English[^english]. This goal enables the
translation to different languages because many people would probably know or
Spanish or English, because they are two of the most widely spoken languages in
the world, and that way they would be able to translate the books to their
mother tongue language to help their own community. I'm not able to supervise a
translation to a language I don't know, but I'm able to make a reliable
material in both of these languages for people to work on top of.

As you see, the goals have a really interesting point of contradiction: I want
to provide technical material for people that doesn't talk English, but I'm
trying to make it in English for it. Funny, huh?

I'm just trying to be as practical as I can.

There are many points I'd like to consider, many other translations I'd love to
do, but I need to focus my effort on something useful in the short term because
if it happens to be useful it's going to push me to keep working on it in the
future, providing more and more material and translations.

[^english]: Don't worry, I know my English is bad and I'm not going to
  translate them, a professional service will (under my supervision for the
  terminology and stuff).

## The feelings

I like the idea of crowdfundings since I heard about them and I've been
planning to make one for years. Electronic devices, software, miniature games
and collections... Everything was candidate for a crowdfunding campaign in my
mind, but I didn't want to disappoint the patrons and I never started one.

This time I think it's possible to provide good material. The content is
already defined and tested in my courses, it only needs to be updated, the
goals are simple and doable, and I have all the good people around me that is
going to help me with everything.

This project helped me connect with the people I love and that's one of the
best things in life. I know everything is going to be fine with their help and
support.

Since I started with ElenQ Technology I had the chance to meet many good people
with incredible skills and love in their heart. This project is somehow the
result of the love they gave me because it filled me with the courage I needed.

Just wanted to express my gratitude[^gratitude].

Thank you all.

[^gratitude]: I don't know why... I'm like that I guess.

<hr style="border-width: 1px; height: 4em;">

#### If you want to take part...

There are many ways you can help but crowdfunding campaigns look like the only
one is giving money and that's not true. Also, there are many ways to give
money and some are more effective than others.

For people that is not able to provide funds but want to take part there are
also very helpful tasks:

- Sharing the campaign with people, communities, universities, libraries and so
  on, that might be interested is always good.

- Once the contents are released in our repository, reviewing the content or
  improving it will help us a lot.

- Translating the content to other languages will help other communities we
  can't directly help. Your language skills are valuable.

- Your love and support is always welcome and it helps us to keep on the good
  job.

For the people that want to provide monetary help, there are points to consider:

- The best way to help is to give money and don't ask for any physical good
  (the first reward is for that) because the books have an associated printing
  and shipping cost. They are going to be released online anyway so if you
  don't really like the idea of having a physical object, you can also take
  part and get the result of the campaign.

- The second best way is to give money and ask for the physical good(s). In the
  case of this campaign, more books you order the cheaper their production is
  (bulk orders and scale economy, you know...).

- One of the highest costs is the shipping, asking us to get the goods in
  person[^coffee] (Bilbao) or making bulk shippings reduces the costs and makes
  the donation more efficient. It's better for us if a group of friends make
  just one big order than having many small orders.

That said, here goes the link to the campaign if you want to take part.

<http://en.goteo.org/project/elenq-publishing>

Thank you.


[^coffee]: If you get the books in person I'll take a coffee/tea with you.
