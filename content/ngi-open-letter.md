Title: The European Union must keep funding free software
Date: 2024-07-20
Category:
Tags:
Lang: en
Summary: I sign the petition urging European Union to continue funding free
    software.

No need to say that during the last 2 years part of my efforts trying to give
people a better world, using my engineering skills, have been funded by the
European Union with the NGI programme, which I was granted by NLNet. Every talk
I give about [what we achieved](https://ekaitz.elenq.tech/bootstrapGcc15.html)
I thank them because without this money I would never be able to use my time in
a long-term project like this.

In [a recent
interview](https://www.ngi.eu/ngi-interviews/ekaitz-zarraga-risc-v-bootstrapping-effort/)
I mentioned many funding programmes out there focus the attention in the
private sector. I am very aware of this because I actually worked as a research
and development engineer for the private sector. Our R&D department was totally
funded by public programmes, to the point we even had profits, but everything
we did was just for the company we were part of. Nothing was released as free
software.

Maybe you didn't know this, but I left that company because I believed (and I
continue to believe) the projects we started to do were not ethical. Our
company, trying to become profitable (task they failed at, years later I left),
started to make projects that used public funding to track users wherever they
went, and harass them with aggressive advertisement. As R&D engineers, our job
was to start to prototype this, which I didn't want to do.

There were many reasons behind my decision, of course, but this was one of
them. I couldn't do that. **That was bad**. And it was done with **your
money**.

I don't know if that model of throwing money to private companies hoping they
become stronger in the current globalised market makes any sense, that's not
for me to know. I'm *just* an engineer. I just hoped that if our money was
given to someone it was to be used for our benefit or at least not to be used
against us.

My personal experience, which I admit is anecdotal but it is shared by many
colleagues, is the programmes that focus on the private sector don't make sure
this happens. They don't even require the projects developed with the money to
be shared with the public, letting the society benefit from the resulting
technical innovation they pledge to be promoting.

I would prefer if the money was spent on things that would be shared, with open
licenses, with everyone. But I'm *just* an engineer, and I don't know about
*economic impact*, and the measurements the people who decide things use for
taking their decisions.

What I do know is the people that really work for others, that make things
because their heart tells them to, are those that would never do something like
what the company I worked for did. They wouldn't think sharing their work as
free software is a project requirement, imposed by the programme they are
funded from. They would do it because they believe it is correct to do it. They
would do it because it is an atrocity not to do so.

Surprisingly, a programme that shared those values existed: NGI; and I had the
honor to be supported by it for the last two years. This has let me focus in
making that my heart believes is correct and my brain has the skill to do. I'm
giving back what I was given by the public university where I studied, by all
the free software I use everyday and by all the open knowledge I have the
privilege to be able to acquire from The (open) Internet.

The NGI programme is important. It changes the world in the proper direction.

It seems the European Union is more interested in putting money in other things
(like AI) instead, maybe not being aware that the world's software
infrastructure has to be maintained, and companies are not going to do it for
us.

Because all that I said, I sign the open letter by Petites Singularités you can
read below.

> Initially published by [petites
> singularités](https://ps.zoethical.org/pub/lettre-publique-aux-ncp-au-sujet-de-ngi/).
> English translation provided by
> [OW2](https://www.ow2.org/view/Events/The_European_Union_must_keep_funding_free_software_open_letter).

### Open Letter to the European Commission

Since 2020, Next Generation Internet ([NGI](https://www.ngi.eu)) programmes,
part of European Commission's Horizon programme, fund free software in Europe
using a cascade funding mechanism (see for example NLnet's
[calls](https://www.nlnet.nl/commonsfund)). This year, according to the Horizon
Europe working draft detailing funding programmes for 2025, we notice that Next
Generation Internet is not mentioned any more as part of Cluster 4.

NGI programmes have shown their strength and importance to supporting the
European software infrastructure, as a generic funding instrument to fund
digital commons and ensure their long-term sustainability. We find this
transformation incomprehensible, moreover when NGI has proven efficient and
economical to support free software as a whole, from the smallest to the most
established initiatives. This ecosystem diversity backs the strength of
European technological innovation, and maintaining the NGI initiative to
provide structural support to software projects at the heart of worldwide
innovation is key to enforce the sovereignty of a European infrastructure.
Contrary to common perception, technical innovations often originate from
European rather than North American programming communities, and are mostly
initiated by small-scaled organizations.

Previous Cluster 4 allocated 27 million euros to:

- "Human centric Internet aligned with values and principles commonly shared in
  Europe" ;
- "A flourishing internet, based on common building blocks created within NGI,
  that enables better control of our digital life" ;
- "A structured ecosystem of talented contributors driving the creation of new
  internet commons and the evolution of existing internet commons".

In the name of these challenges, more than 500 projects received NGI funding in
the first 5 years, backed by 18 organisations managing these European funding
consortia.

NGI contributes to a vast ecosystem, as most of its budget is allocated to fund
third parties by the means of open calls, to structure commons that cover the
whole Internet scope - from hardware to application, operating systems, digital
identities or data traffic supervision. This third-party funding is not renewed
in the current program, leaving many projects short on resources for research
and innovation in Europe.

Moreover, NGI allows exchanges and collaborations across all the Euro zone
countries as well as "widening countries" [^1], currently both a success and an
ongoing progress, likewise the Erasmus programme before us. NGI also
contributes to opening and supporting longer relationships than strict project
funding does. It encourages implementing projects funded as pilots, backing
collaboration, identification and reuse of common elements across projects,
interoperability in identification systems and beyond, and setting up
development models that mix diverse scales and types of European funding
schemes.

While the USA, China or Russia deploy huge public and private resources to
develop software and infrastructure that massively capture private consumer
data, the EU can't afford this renunciation. Free and open source software, as
supported by NGI since 2020, is by design the opposite of potential vectors for
foreign interference. It lets us keep our data local and favors a
community-wide economy and know-how, while allowing an international
collaboration.

This is all the more essential in the current geopolitical context: the
challenge of technological sovereignty is central, and free software allows to
address it while acting for peace and sovereignty in the digital world as a
whole.

[^1]: As defined by Horizon Europe, widening Member States are Bulgaria,
    Croatia, Cyprus, the Czech Republic, Estonia, Greece, Hungary, Latvia,
    Lituania, Malta, Poland, Portugal, Romania, Slovakia and Slovenia. Widening
    associated countries (under condition of an association agreement) include
    Albania, Armenia, Bosnia, Feroe Islands, Georgia, Kosovo, Moldavia,
    Montenegro, Morocco, North Macedonia, Serbia, Tunisia, Turkey and Ukraine.
    Widening overseas regions are : Guadeloupe, French Guyana, Martinique,
    Reunion Island, Mayotte, Saint-Martin, The Azores, Madeira, the Canary
    Islands.
