Title: Takerufuji made history
Date: 2024-04-01
Category: Aprilcools
Tags:
Slug: takeru
Lang: en
Summary:
    This March we witnessed history of sumo. What **Takerufuji** did what seemed
    impossible.

<style>
figure{
    margin: auto;
    width: 300px;
}
figure img{
    margin: 0;
    height: auto;
    width: 100%;
}
figure figcaption{
    padding: 1rem;
    font-size: 90%;
    font-style: italic;
    width: 100%;
}
</style>


**Takerufuji** Mikiya, born as Ishioka Mikiya in 1999, loves sumo. He does
amateur fights at university, where he finds **Ōnosato** Daiki, 2000, formerly
known as Nakamura Daiki, the most prominent young fighter in top-level sumo
today, a worthy rival.

It's 2017, **Terunofuji** Haruo, born in Ulaanbaatar, Mongolia, as Gantulgyn
Gan-Erdene in 1991, is a top-level *ōzeki*, the second highest sumo range. He
is strong as a monster, lifting other fighters and carefully placing them out
of the *dohyō*, the sumo "ring", almost without resistance. He is an absolute
display of strength, but he's not immune to injury. After several knee injuries
and health problems, he is not able to fight for so long he is relegated to
lower sumo ranges, forced to fight in a lower division each competition he
misses. He is forced to start in *jonidan* when he returns in 2019. That's the
fifth division in sumo.

<figure>
<img src="{attach}/sumo/terunofuji.jpg">
<figcaption>Terunofuji in 2023 (wikipedia)</figcaption>
</figure>

**Terunofuji** is back in business. During his first competition after the
return he secures the promotion to the *sandanme*, next he is promoted to
*makushita*. Two *makushita* competitions later, with a 6-1 record, he wins the
*makushita* competition with an amazing 7-0, securing the promotion to *jūryō*,
which he also wins (13-2) once and has a very good performance next (10-5).
He's back in *makuuchi*, the top level division. It's 2020.

During the first top-level division competition after his return,
**Terunofuji** wins the competition (13-2), also obtaining the special prizes
for *Outstanding Performance* and *Technique*. One year later, he is back at
*ōzeki* range, winning three of the six competitions he took part in during the
year, and being in the runner-up on five of them. After a great performance
(14-1) he is promoted to *yokozuna*, the highest range in sumo, reserved only
for the grandmasters of the sport. In 2021, after a two-year comeback, he
became the 73rd *yokozuna* in the recorded sumo history (since 1798).

The young **Takerufuji**, who is still fighting in amateur sumo, decides to
make the jump to professional, inspired by **Terunofuji**'s comeback. In
November 2022, he wins (7-0) his first *jonokuchi* competition, the lowest
division in sumo. He is promoted to *jonidan*, that he also wins (7-0). Next he
makes a great performance in *sandanme* (6-1) and is promoted to *makushita*
where he obtains very good results (6-1, 6-1, 5-2 and 6-2) and is finally
promoted to *jūryō*, the second division. Now, he can consider himself a proper
professional. It's January 2024. Only a little bit more than a year passed
since he started his professional attempt.

<figure>
<img src="{attach}/sumo/takerufuji.jpg">
<figcaption>Takerufuji in 2024 (wikipedia)</figcaption>
</figure>

**Ōnosato** Daiki has also made his bet. In May 2023 he started at *makushita*
level, as he was a top fighter in amateur sumo, he could skip lower divisions.
After two good performances in *makushita* (6-1 and 4-3) he is promoted to
*jūryō* and continues to perform at the same level (12-3 and 12-3) and is
finally promoted to the top division, with a *maegashira 15* range, in January
2024. In less than a year, thanks to his exceptional strength and technique,
**Ōnosato** signs the third-fastest to reach the top division since 1989. He
was so fast, his hair hasn't grow enough yet to tie in a *chonmage*, the
mandatory sumo hairstyle.

<figure>
<img src="{attach}/sumo/onosato.jpg">
<figcaption>Ōnosato in 2023 (wikipedia)</figcaption>
</figure>

While **Ōnosato** is doing a great performance in his top division debut, in
January 2024, ending with a great 11-4 record, **Takerufuji** is almost
unstoppable in *jūryō*, winning the championship with an excellent 13-2 record
and being thus promoted to the top-level division, where both young fighters
would meet, again since their amateur times, in March.

It's March 2024, and the time has come. Both **Ōnosato** and **Takerufuji**
have a great start, with 0 losses the sixth day. After the first two thirds of
competition, the tenth day, both arrive with very good numbers: **Ōnosato**
with a 8-1 after his loss against **Ōnoshō** the seventh day and **Takerufuji**
has a surprising non-loss streak, very rare on first division debutants, 9-0
that is.

Both having a similar range, and a very good record, are paired together.
**Ōnosato** is 40 kilograms heavier, he is very strong and his technique is
sharp. **Takerufuji** is 8 centimeters shorter, and strong as a truck. In their
first duel in the sumo top division, hopefully from many, **Ōnosato** is
blasted out of the ring, and **Takerufuji** obtains his 10-0. Since the 15-day
tournament format was established in 1949, only the *yokozuna* **Taiho**
conquered an eleven-win streak in his first top-level competition.

Next day, **Takerufuji** fights **Kotonowaka**, a young fighter at the *ōzeki*
range, winning the fight and matching **Taiho**'s record. After this, he
demonstrated he could challenge the hardest fighters in the competition. Next
day **Hōshōryū** comes, the young Mongolian fighter is one of the most feared
in the *ōzeki* range. **Hōshōryū** is fast, strong and his technique, based on
traditional Mongolian wrestling, *bökh*, is both accurate and hard to defend
against.

After a very intense start, **Hōshōryū** lands a perfect technique and throws
**Takerufuji** out of the *dohyō*, putting an end to the 11 win streak, but not
yet to the competition.

The 14th fight comes and **Takerufuji** has a 12-1 record, the best from all
the participants. This time loses against **Asanoyama** injuring his ankle
right at the beginning of the fight. 12-2 that is, and everybody is afraid he
won't compete the last day, as he couldn't walk in his way out.

The last day of the competition starts, **Takerufuji**, with his 12-2 is one
win away from the next fighter, **Ōnosato**, who secured a great 11-3 record
and is one win away from the next in the list: **Hōshōryū**.

**Takerufuji** has an injured ligament, and his master has advised him not to
fight. Nobody knows what would happen. When his turn comes, he walks to the
center of the *dohyō* with a bandage in his right ankle. Even if he loses he
would still have chances to win the tournament if **Ōnosato** loses his fight
against **Hōshōryū**, but we all know he will try to win this fight against
**Gōnoyama**, his spirit is one of a warrior.

**Gōnoyama**, a great fighter (10-4 at the fight), has a great start, almost
dropping **Takerufuji**, when he put too much weight in his injured ankle. But
**Takerufuji** holds in place, grabs **Gōnoyama**'s *mawashi* and push, push,
push until he is out. **Takerufuji** wins the competition and becomes the
second fighter that wins the *makuuchi* competition as a debutant since
**Ryogoku**, who won the summer tournament of 1914, that's 110 years.

The tournament continues, as some fights are left. **Ōnosato** has to fight
**Hōshōryū**, the only sumo that was able to win against an uninjured
**Takerufuji**. In a spectacular display of technique and flexibility,
**Hōshōryū** throws **Ōnosato** out of the *dohyō*. 11-4 for **Ōnosato** in the
end, signing a great competition and also wining  *fighting spirit* and
*technique* special prices.

**Takerufuji**, also received all possible special prices: *fighting spirit*,
*outstanding performance* and *technique*; and he is honored as the great
warrior he is. His name is now written in sumo history forever.

Many have no doubt that **Takerufuji**, **Ōnosato**, and probably **Hōshōryū**,
will become great *yokozuna*s one day, but sadly, the only one we have now, the
inspiration for this huge accomplishment, **Terunofuji**, was out of the
competition, after a very poor start, due to his injuries and health problems
at his 32 years of age.
