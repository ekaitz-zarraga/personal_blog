Title: ElenQ Publishing
Date: 2020-02-18
Category: ElenQ Publishing
Tags:
Slug: elenq-publishing
Lang: es
Summary:
    ElenQ Publishing es un proyecto de publicaciones técnicas que pretende abir
    la puerta del conocimiento técnico a cualquiera.


Saludos,

Hoy 18 de febrero de 2020 ha empezado la campaña de ElenQ Publishing y me
gustaría hablar un poco sobre ello.

<http://goteo.org/project/elenq-publishing>


## La plataforma

En primer lugar, me gustaría destacar la plataforma en la que se ha publicado:

<http://goteo.org>

Goteo es una plataforma para campañas de mecenazgo con un fondo social.
Funciona sobre código libre publicado con licencia Affero GPL, que permite la
reutilización y la extensión del mismo siempre y cuando el código de la
plataforma (tanto servidor como cliente) esté disponible para <del>los
usuarios</del> las personas que la usen.

La Fundación Goteo se encarga de mantener el código de goteo y de gestionar
goteo.org. La fundación se financia con el 5% del dinero obtenido por las
campañas, una cifra bastante justa por sus servicios, y por el apoyo que
reciben de entidades públicas como el Ayuntamiento de Barcelona y el Ministerio
de Educación, Cultura y Deporte de España.

Además, al menos en España y no sé si en otros países, las personas que
participen aportando dinero en campañas de Goteo pueden declararlo en su
Declaración de la Renta y desgravar por donación a fines sociales.

Para los que quieran hacer una campaña, la Fundación Goteo revisa el contenido
y aporta recomendaciones e ideas. Además, la plataforma está pensada para
aceptar varios idiomas y la interfaz está traducida a todos los idiomas
regionales de España y algunos otros más.

En general, encaja muy bien con la perspectiva ética de ElenQ Technology.

## La historia

No hablo mucho sobre ello, quizás menos de lo que debería, pero he estado
dedicándome a dar cursos relacionados con la informática durante estos primeros
3 años de ElenQ Technology. He dado cursos de diversos temas: introducción a
python, python avanzado, bitcoin y blockchain[^blockchain], clojure... Y
algunos más que no recuerdo. He trabajado en muchos contextos distintos desde
cursos para jóvenes en desempleo a cursos para ingenieros en centros de
investigación. Parece, además, que voy a seguir haciéndolo, porque es un
trabajo que disfruto y los alumnos suelen decirme que se me da bien.

[^blockchain]: No me juzguéis demasiado rápido: el curso estaba enfocado desde
  una perspectiva técnica que trataba de fomentar el pensamiento crítico en los
  tiempos del boom del blockchain.

Por mucho que me ayude a ganarme el pan, creo que esto no es suficiente. En mi
día a día veo muchos problemas que me gustaría resolver.

Me dí cuenta que tengo materiales y cursos ya probados que quiero compartir.

Me dí cuenta que la gente que no estudia estos temas por su cuenta no lo hace
por vagancia o porque no sean suficientemente inteligentes. Muchos de ellos no
lo hacen porque tienen problemas con el inglés y necesitan a alguien como yo
que les sirva de puente. Están aislados por el lugar en el que nacieron o por
la cultura que tienen. Esto es inadmisible.

Me dí cuenta que las publicaciones técnicas que leía estaban escritas, en
general, desde la misma perspectiva. Tiene sentido, porque la mayor parte de
los autores parten del mismo contexto. Me gustaría tener unas publicaciones
técnicas más diversas y la única forma de conseguir esto es hacer que la
tecnología sea más accesible.

Me dí cuenta de que en mi contexto local el sistema educativo tiene problemas
evidentes que parece que no hay ningún interés en resolver:

- En mi provincia, los cursos subvencionados sólo se centran en colectivos que
  es probable que consigan un trabajo en el corto plazo. De este modo, los
  políticos responsables pueden decir que consiguieron el trabajo gracias a
  ellos y seguir ganando elecciones. Los jóvenes que acaban de terminar la
  universidad conseguirán un trabajo en el corto plazo independientemente de
  que realicen o no realicen cursos subvencionados. Esto no significa que no
  los necesiten[^cursos], significa que no afectará a su empleabilidad. ¿Qué
  pasa con quienes tienen **verdaderos problemas** para conseguir
  empleo[^problemas]?

- Algunas personas tienen problemas individuales que no encajan en los
  objetivos de las campañas sociales de gobiernos y otras entidades porque se
  fijan en grandes grupos de personas con problemas similares y no en personas
  individuales. Tiene sentido que lo hagan, porque de este modo su ayuda es más
  eficiente, pero esta red de soporte tiene orificios que deberíamos resolver.

- No hay soporte estructural para personas que simplemente quieren aprender por
  el placer de hacerlo y no con un fin laboral. La universidad está derivando.
  Hoy en día no es mucho más que un lugar que te da un papel con el que luego
  es más fácil conseguir un trabajo, pero no está satisfaciendo las necesidades
  intelectuales de la sociedad como debería. Ya no es un lugar donde encontrar
  conocimiento. Muchas personas no quieren aprender ni pensar, pero otras sí
  que quieren y les estamos impidiendo hacerlo.

- En otros lugares el problema de la educación es incluso peor y no tienen
  recursos (o voluntad[^voluntad]) para solucionarlo. Las personas no deberían
  sufrir las consecuencias de un sistema que no funciona, sea por la razón que
  sea. Es nuestra responsabilidad ayudar a todo el mundo a tener oportunidades
  para desarrollarse tanto como quiera independientemente de su contexto.

En general, todos estos puntos vienen a resumirse en uno: El conocimiento tiene
que ser libre. Si no es libre no se puede considerar conocimiento, es sólo algo
que me hace más fuerte que los demás: es injusticia.

Me dí cuenta que todas estas cosas pueden solventarse (o tratar de solventarse)
con un buen repositorio de conocimiento en varios lenguajes y, ya que doy
clases y me gusta escribir, considero interesante dedicar algo más de tiempo a
los apuntes que entrego a mis alumnos y darles forma de libro.

Con [un poco de esfuerzo](https://ekaitz.elenq.tech/templates-released.html),
puedo crear un libro que se puede publicar en la web, en un libro físico y un
PDF fácil de imprimir y de copiar en una copistería cercana. Puedo hacer que
esto llegue a cualquier lugar del mundo.

No sólo eso. Alguien se ha tomado la molestia de crear una licencia que permite
que los contenidos que publique sean editados y mejorados siempre que el
producto resultante tenga la misma condición: Creative Commons.

Por tanto, con un poco de esfuerzo y un poco de presupuesto (y una sonrisa)
puedo crear un proyecto de publicación que albergue el conocimiento que a
diario me encuentro gracias a mi trabajo y poder así compartirlo de forma ética
y accesible, respetando las circunstancias de las personas que quieran
consumirlo.

Esto es algo que, evidentemente, quiero intentar. Es algo que *tengo que*
intentar.

[^cursos]: Los necesita, sobre todo si son cursos como los que yo hago en los
  que hablo de ética y de cómo crear tecnología independiente :D

[^problemas]: Hablo de las mujeres, de los desempleados de más de 50 años que
  fueron expulsados de sus puestos de trabajo por la desindustrialización, las
  personas con discapacidad, inmigrantes, las que acaban de salir de la
  cárcel...

[^voluntad]: Un saludo para los Estados Unidos de Norteamérica.

## La campaña

Es por eso que lo estoy intentando.

Esta campaña es el primer intento para hacer de esto una realidad. Si tiene
éxito, me permitirá dedicar un poco de tiempo a dar amor a los contenidos que
quiero publicar y me permitirá publicar el conocimiento de otros.

El objetivo de publicar libros físicos es sólo un vehículo para poder
publicarlos de modo que sea fácil de compartir. Los objetos físicos son sólo
una forma de conseguir los fondos.

La idea principal no es hacer unos libros, es crear una infraestructura para
compartir contenido que me permita compartir lo que investigo y pueda servir de
plataforma para que otros compartan lo que ellos investigan. Todo el contenido
será publicado en un crudo en un repositorio que cualquiera pueda auditar,
revisar, mejorar o crear proyectos derivados desde éste.

Una vez que la infraestructura esté disponible, publicar nuevos contenidos será
extremadamente sencillo. El primer proyecto nos enseñará a tratar con el
papeleo necesario para conseguir los ISBN, el registro del libro, etc. y para
crear las herramientas necesarias para publicar (una web...). Una vez resueltos
estos puntos, el resto es "sólo" escribir y publicar.

Los objetivos de la campaña están separados en dos niveles: el mínimo y el
óptimo.

El mínimo trata de publicar los libros en español, el idioma en el que pienso,
y cubre los gastos de infraestructura para estos. De este modo, todas las
personas de habla hispana podrán tener libros técnicos en su idioma.

El objetivo óptimo asegura la publicación de los libros en inglés[^ingles] con
el fin de habilitar la traducción a otros idiomas. Muchas personas son capaces
de hablar, además de su propio idioma, inglés o español, ya que son dos de los
idiomas más hablados del mundo. De este modo, la probabilidad de que alguien
pueda tomar nuestras publicaciones y traducirlas a otros idiomas aumenta de
forma radical, facilitando así que ayuden a sus comunidades de una forma en la
que nosotros no estamos capacitados. Puedo hacer lo posible para ayudar en
traducciones a los idiomas que conozco, pero no tengo más alcance que ése.
Aportar una buena base en un idioma común permite que estas traducciones
espontáneas surjan de forma independiente.

Me encantan estas contradicciones: quiero aportar material para acabar con la
hegemonía del inglés y lo publico en inglés. Es gracioso. ¿Verdad?

Sólo intento ser lo más práctico posible y elegir qué batallas puedo librar.

Hay muchas cosas que me gustaría revisar, muchas traducciones que me gustaría
hacer, etc. pero necesito fijarme en lo que puedo aportar a corto plazo y, si
resulta útil, crecer desde ahí, ya que me dará fuerzas para seguir trabajando
en el futuro.

[^ingles]: Tranquilidad, no seré yo el traductor. Soy consciente de que mi
  nivel de inglés no es suficiente para una tarea así. Sí que trataré de
  supervisar que la terminología, etc. es la correcta, pero no llego a mucho
  más.

## Los sentimientos

La idea de los crowdfunding lleva años en mi cabeza. Llevo años tratando de
hacer alguno: de electrónica, software, juegos y colecciones de miniaturas...
Todas mis aficiones eran candidatas a ser un crowdfunding, pero nunca me animé
a hacerlos porque no quería decepcionar a los mecenas. Me daba vértigo.

En esta ocasión creo que puedo aportar buen material. El contenido está probado
en mis cursos, sólo necesita actualizarse, los objetivos son simples y
realizables y estoy rodeado de buenas personas que me ayudan con todo.

Este proyecto me ha ayudado a conectar con las personas que quiero y eso es
algo maravilloso. Sé que todo va a salir bien con su ayuda y su apoyo.

Desde que empecé ElenQ Technology, he tenido oportunidad de crear vínculos con
gente maravillosa a la que tengo mucho respeto. Este proyecto es, de alguna
manera, el resultado del amor que me dan, porque me ha hecho superar el miedo
al fracaso. Con su ayuda, creo que puedo conseguir todo lo que me proponga.

Sólo quería expresar mi gratitud[^gratitud].

Gracias.

[^gratitud]: Soy así, yo qué sé.


<hr style="border-width: 1px; height: 4em;">

#### Si quieres colaborar...

Hay muchas formas de colaborar pero la realidad es que los crowdfunding dan la
impresión de que la única es la monetaria. Eso no es cierto. Además, algunas
formas de aportar fondos son más efectivas que otras.

Para las personas que quieran ayudar sin hacer aportaciones monetarias hay
unas tareas que serían muy útiles:

- Compartir la campaña con personas, comunidades, universidades y librerías que
  puedan estar interesadas siempre es útil. Yo prefiero compartir con quien
  pueda tener interés más que insistir de forma aleatoria en las redes
  sociales.

- Una vez los contenidos estén disponibles en el repositorio, revisarlos y
  mejorarlos nos ayuda mucho. La tarea de revisar libros es tediosa y aburrida
  y cualquier ayuda que podamos tener ahí será bienvenida.

- Traducir el contenido a otros idiomas es importante. Nosotros a título
  personal no podemos ayudar en esa tarea, si lo haces, ayudas a tu comunidad
  de forma directa.

- Tu amor y tu apoyo es siempre bienvenido y nos ayuda a tener fuerzas en los
  momentos donde el trabajo se acumula.

Para los que quieran y puedan permitirse una ayuda monetaria, hay algunos
detalles a considerar:

- La mejor manera de ayudar es no pedir el producto físico (para eso está la
  primera recompensa). De este modo, ayudas sin crear ningún tipo de gasto
  material y de igual modo podrás acceder al contenido una vez se publique.
  Evidentemente, los libros físicos tienen algo de romanticismo y por eso se
  ofertan.

- La segunda mejor manera es pedir los productos físicos. En el caso de esta
  campaña, cuantos más libros produzcamos menos coste tendrá cada unidad
  (economía de escala).

- Uno de los mayores gastos de la campaña es el envío. Recoger los libros en
  mano[^cafe] (Bilbao) o agrupar pedidos reduce el coste del envío y hace que
  la donación sea más eficiente. Es mejor para nosotros (y para ti, debido a
  los descuentos) si se forma un grupo de personas y hacen pedidos conjuntos.

Dicho esto, aquí va el link de la campaña por si te apetece participar:

<http://goteo.org/project/elenq-publishing>

Muchas gracias.

[^cafe]: Si te acercas a la ciudad para recibirlo en mano te invito a un café o
  un té y charlamos un rato si te apetece.
