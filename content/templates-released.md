Title: Let's document
Date: 2019-02-01
Category:
Tags:
Slug: templates-released
Lang: en
Summary:
    ElenQ Technology document templates released, with a long writeup
    about the workflow we use with them.

At [ElenQ Technology][elenq] I just released my documentation templates tool.
You can find it in the link below:

[https://gitlab.com/ElenQ/templates](https://gitlab.com/ElenQ/templates)

I think that project, even if it's quite simple, it's a really good reason to
talk about document edition and my workflow.

## The story

As part of my job at ElenQ Technology I document a lot of things: I have to
make reports, proposals, documentation for projects, notes for courses...

I have to write **a lot**.

If I could decide, I'd share all my documentation and files in plain text. But
I don't decide, so I **need** to send PDF files and they need to have a nice
look so the clients understand I take care of my stuff.  I also like to pay
attention to the aesthetics of what I do so I really like to keep everything in
order.

That's really difficult to do. Even more if you work with tools like
LibreOffice that have tons of options and menus and are sometimes difficult to
understand or hard to make them do exactly what you want. I have nothing
against LibreOffice but some years ago I realized it's not a tool for me.
WYSIWYG[^1] tools like that have some characteristics that don't fit my
workflow well. Let me break them down:

- They are designed to work with a mouse, and I avoid using the mouse because
  it makes my wrist and arm hurt. That's why I often work with my wacom tablet
  in mouse-intensive tasks like PCB routing and I use laptop's touchpad in
  everyday tasks.

- They have tons of menus where you can get lost while the most of the
  documents you write don't have that kind of complexity. Often, that kind of
  options just make the documents complex and hard to maintain.

- They don't have a clear separation between the content and the view. When I
  write I like to focus on the content and avoid to get distracted with how it
  looks on the screen. I hate "Oh my god, the picture moved and now the whole
  layout is broken"-like errors.[^2]

- Their file formats are difficult to operate with even if they are open
  standards.  Mixing data with something that comes from a different process is
  really complex, and it makes the user write everything by hand.  
  As an example of this: in the previous version of the [ElenQ Documentation
  Templates][old-version], there was a tool to get all the git tags of the
  project and insert them as a document changelog. This is really difficult to
  make in LibreOffice. (This version doesn't support that *yet*).


Trying to solve all those issues, I've spent some time with LaTeX as a main
tool but, it also has a really thin separation between the content and the view
and its learning curve is crazy hard.

## Enter pandoc

Some day, while working on her PhD, my sister discovered **Pandoc** and our
life changed.

[Pandoc][pandoc] is a great tool which is able to convert between a lot of
different document formats. That opens a world of possibilities where you can
write in a format you like and then convert it to different output formats.
It's huge. The main power of Pandoc is also related with the amount of output
formats it can handle. It is possible to write all the content of a document in
a common language like MarkDown, RST or AsciiDoc and then convert it to
different output formats like PDF, ePub or to a simple static website.

All this tooling also lets you write files that are easy to write and read,
like MarkDown is, without the need to play with tons of tags and weird commands
like HTML or LaTeX do.

Pandoc is a really powerful tool with tons of option that can be quite
overwhelming. It even lets you add filters that transform the AST it creates
during the conversion!

At the moment we discovered Pandoc I was really obsessed with productivity and
the chronic pain my hands, wrists and arms were suffering and I didn't care
about anything else. If a tool could help me reduce the use of the mouse and
my keystroke count it was worth the time learning it.

I was so crazy at that time that I made a Vim plugin called
[droWMark][droWMark] for posting in WordPress. Taking advantage of Pandoc
filters I also made it able to [upload images][droWMark-images] linked from the
MarkDown file. It was fun.

## Choose the tools

Some time later I founded ElenQ Technology and I decided we needed to integrate
Pandoc in our tooling. That's why with my sister's help we created the first
version of the [documentation templates][old-version].

I wanted any person working with the documents to be able to use the editor
they like the most. And I only wanted to care about the aspect of the document
once: during the template creation.

It worked. I spent almost 2 years working with the old version of the templates
and they served me well. The only problem they had was that they needed many
files to work and they added some complexity to the folder where the
documents were edited.

## Choose the tools: remastered

This new version eliminates that complexity. We needed to sacrifice a couple of
features but now there's no need to add any extra file in the directory where
the document is. We removed the Makefiles and embedded the SVG logo of the
company inside the templates using TikZ. Now the tool is just a couple of
Pandoc LaTeX templates: `elenq-book` template for long documents and
`elenq-article` for short documents.

Like in the previous version, both templates are designed to create output
LaTeX files that can be compiled to PDF using XeLaTeX (or let Pandoc do the
conversion for you). The input formats are not defined, the only limitation is
on the metadata they need (you can read the documentation included with the
project for that).

All of this is installed *automagically* using [Stow][stow].

The project also explains in the `README.md` file how to create a couple of
command line aliases to simplify the calls to Pandoc. You really want to use
them because Pandoc needs *a lot* of input arguments. Using aliases, the
conversion as simple as running a command in the terminal:

    ::bash
    elenqdoc-book document.md -o book.pdf           # For books
    elenqdoc-article document.md -o article.pdf     # For articles

With the new template system, the documents are just Markdown files and they
are easy to maintain under version control. Note that the same input file can
be used to create an article and a book, the input doesn't really affect the
output of the process.

We decided to use MarkDown for some extra reasons too. Markdown is simple but
has everything that any simple document needs and it's easy to read in plain
text even for people who don't know it. But not only that, MarkDown is a widely
use format ([this blog][blog-md] is written in MarkDown too!) and it's really
extensible, letting the user insert HTML or LaTeX pieces to cover specific
cases like formulas or complex formatting.

## Choose the tools: future chapters

Next step is the creation of a invoice control system that is integrated with
the Pandoc templates. The template integration is really easy, we only need to
inject some variables to the templates and Pandoc already has a tool for that:
the metadata system. From that side the problem is solved, now we need to make
all the rest.

On the other hand, as said before, in the future, if the conversion process
needs extra complexity, we'll just need to add some Pandoc filters to create
it.

## Wrapping up

In summary, we can say that the tool we made is just a consequence of the
workflow we follow. This is probably not for anyone, but any person used to
work with the terminal and software is a potential user for this kind of tool.

It's powerful, simple and straight-to-the-point. I think that fit's our
workflow really well.


[elenq]: https://elenq.tech/
[pandoc]: https://pandoc.org/
[droWMark]: https://www.vim.org/scripts/script.php?script_id=5374
[droWMark-images]: https://github.com/ekaitz-zarraga/droWMark/issues/2
[old-version]: https://gitlab.com/ElenQ/documentation-templates
[stow]: https://www.gnu.org/software/stow/manual/stow.html
[blog-md]: https://gitlab.com/ekaitz-zarraga/personal_blog/raw/master/content/posts/templates-released.md

[^1]: WYSIWYG: What You See Is What You Get
[^2]: Obligatory xkcd reference: [https://xkcd.com/2109/](https://xkcd.com/2109/)
