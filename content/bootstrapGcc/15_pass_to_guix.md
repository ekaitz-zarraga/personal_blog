Title: Milestone (End?) - Bootstrapping path discovered
Date: 2024-07-02
Category:
Tags: Bootstrapping GCC in RISC-V
Slug: bootstrapGcc15
Lang: en
Summary: A RISC-V bootstrapping path path has been discovered. Now it's time
    for the distros to catch up.


During the latest posts I described that we managed to build GCC using our
bootstrapping chain, and also that we built a modern GCC using our bootstrapped
one, but now, some connection work has been done. It's time to report on that.

### Bootstrapping chain discovered

In my [`commencement.scm`][commencement] project, I started working on a
bootstrapping path following Andrius Štikonas' steps in live-bootstrap. His
work is what is driving everything for several reasons: he knows how to do it
well and Guix has a handful of peculiarities that make this way more complex
than in live-bootstrap.

First of all, live-bootstrap uses custom Makefiles for most of the projects,
that allows it to build things without relying on the feature detection that
the configure scripts normally use. We could do the same thing on Guix but
that would mean rewriting many packages from scratch and Guix package
definitions would become way more complex than what they are right now.

Second, live-bootstrap is launched in some kind of a chrooted environment that
is shared by all steps. This means all the packages in it share the `/` folder,
and they can easily find other programs in `/bin` and libraries and stuff in
the folders expected by the FHS. In Guix we do *not* support the FHS, and our
packages are isolated, so we need to tweak the environment variables of the
packages in order to make them find the dependencies. This sometimes requires
patching the source of some packages, and here problems appear.

The most heavily patched package in this regard is actually GCC. It needs to be
able to work well with our environment and that's not easy to do. This makes my
job harder.

The good news is Andrius managed to build GCC 9.5 with the bootstrapping chain
in live-bootstrap and that means we discovered a full-source bootstrapping
path for RISC-V in 64 bit that is working **right now**.

[commencement]: https://github.com/ekaitz-zarraga/commencement.scm

### Time to make it reach a distro

I managed to build GCC 9.5 with C support, using our bootstrapping path, and
that's more than nothing, but we are working with Efraim Flashner (one of the
Guix maintainers) to add full support for GCC 9.5 and also Janneke is working
on top of my `commencement.scm` file to merge it with the support they already
did for x86 (and was already done when I joined this effort).

So, the efforts to include our work in Guix are currently happening and I don't
think there's much Andrius and I can do to help in this direction. The project
is on the hands of the people that know the best.

#### Issues

During the project we found some small inconveniences that we could not fix,
and those must be improved but they'll take long time, as they come from some
related projects. I'd love to say I have time, energy and knowledge to fix
those, but most of them are not under my control.

First we have the problem we detected with Gash, that produces hangs in the
very first steps of the bootstrapping process. This is really hard to fix but
Timothy Sample, the project maintainer, is working on the issue.

Second, some of the projects we use in the bootstrapping process require some
packaging skills I lack at this very moment, but they have been bootstrapped in
live-bootstrap so this can be done. The projects are `flex` and `bison`, and
maybe a couple more, but they are out of the scope of the project so I just
used the non-bootstrapped ones and left them as *to-do*s in the
`commencement.scm` file[^issue].

[^issue]: I also left those because we don't build them from source in Guix
    yet. See <https://issues.guix.gnu.org/52311>


### What now, then?

I think what to do next is pretty obvious now: handover to Guix.

I'll open an issue in Guix in the following days where we'll discuss the
inclusion of this bootstrapping path in Guix in the following months, as the
other related issues are fixed and more steps are included in the chain.

This handover process will take time because the bootstrapping path doesn't end
in a modern GCC, but in a *proper* GCC, GLibC, and some other packages that are
required for almost everything. Our discovery is enough to continue further,
but we didn't do that as our goal was to provide the RISC-V support in the
places were it wasn't ready. We have already shown that we did that. **Now I
guess it's time for the distros to catch up**.

Of course, with our help. As always.
