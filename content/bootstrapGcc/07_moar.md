Title: More work, more people, more energy — thanks NlNet
Date: 2023-07-17
Category:
Tags: Bootstrapping GCC in RISC-V
Slug: bootstrapGcc7
Lang: en
Summary:
    Now it's time to focus on combining all the previous work and making it
    production ready. NlNet for the rescue, again.

I might become a little bit famous in this small world of Guix, RISC-V and
bootstrapping after my [FOSDEM talk of this year][fosdem23] and the work I did
during 2021 and 2022, that you can follow in this [series of posts][series] I'm
going on with right now.

All that I write here is nothing I did alone. Many people helped me to make
this happen, but in the end it is me the one that writes here and makes the
noise. So, before explaining anything else, I want to thank everyone that is
involved in the process.

I also want to thank [NLNet / NGI-Assure][nlnet] for funding the project.
Without there wouldn't be anything to discuss here. They just enabled this work
with their funds.

[nlnet]: https://nlnet.nl/project/GNUMes-RISCV/
[fosdem23]: https://fosdem.org/2023/schedule/event/guixriscv/
[series]: https://ekaitz.elenq.tech/tag/bootstrapping-gcc-in-risc-v.html

The work is done, it was funded and it was finished. I backported RISC-V
support to GCC, and I also backported the RISC-V support to the bootstrappable
TinyCC, but that's not enough. All that I did has to be combined with the whole
bootstrapping toolchain, so it's time for more.


### A new way

Even with all the help, during the project I felt alone. The codebases are huge
(GCC is millions LoC), or very badly written (tcc I'm looking at you) and there
are tons of moving parts (Hex0, M1, M2-Planet, Mes, tcc, bootstrappable tcc,
gcc, all the libcs...). It's really hard to know everything and none of us know
all the ecosystem deeply so many times there's none to ask for help. You are
alone.

This might seem a good thing, a challenge, and it is, but it's also very energy
consuming. I did all I could and I'm not sure if I can take this a lot further
by myself.

Now, the project has evolved. We have most of the dots and it's time to draw
the line that connects them.

In order to do that we need more collaboration as each of us has become an
expert in a different part of the chain. Also, many new problems will arise
from the interaction between the different parts.

Knowing that, this time I proposed something else: I wanted to make a larger
project where more people would collaborate and I asked NlNet for the funds to
continue the work from that perspective.

That's good because we can pay every person involved on this according to their
implication[^surprise].

[^surprise]: It might sound really surprising to some, but some of the people
  involved on this are paid zero money for their time at the moment, and they
  are doing great improvements. This is a topic for a huge discussion but, in
  summary: work is work, and you should get paid for it.

### NlNet / NGI Assure

Of course, I wouldn't be writing this if NlNet didn't give us the funds.

So, yes: NlNet decided to fund us. Big thanks to them and to NGI Assure.

<style>
.container{
    display: flex;
    flex-flow: row wrap;
    justify-content: center;
    gap: 40px;
}
.no-side-margin{
    margin:  0px;
}
</style>
<div class="container">
<img class="no-side-margin" src="{attach}/bootstrapGcc/nlnet.svg"     width=200px>
<img class="no-side-margin" src="{attach}/bootstrapGcc/NGIAssure.svg" width=200px>
</div>


### The work

As I introduced in the [Fosdem talk][fosdem23], there's a lot of integration
work to do.

During the last year I focused on backporting the RISC-V support to GCC and the
Bootstrappable TinyCC. I did that because I knew that would enable more work on
the whole chain of compilers we use in the bootstrapping process. But also
because I could just focus on a very specific part, forgetting about the whole
chain for a moment.

Now it's time to start combining all the work together.

The funding includes enough tasks to make the full source bootstrapping for
RISC-V. This is a summary of the tasks:

- Finish GNU Mes' RISC-V support
- Build the Bootstrappable TinyCC using GNU Mes' RISC-V support added in the
  first task
- Fix the backported GCC 4.6.4 package to include C++ support and fix missing
  functionality
- Build the backported GCC 4.6.4
- Build the upstream GCC 7.5 or higher with the backported GCC 4.6.4
- Package the whole process and include it in Guix's commencement module
- Review the associated projects and fix the possible issues
- Document all the process

You are probably not familiar enough with the whole thing to know what they
really mean but some of them are **really hard**.

I'll go into more detail on all of them as we work on them, so don't worry at
the moment.

### The feelings

I'm not very excited with this project anymore. The tasks you can see in the
previous block are not good for a person like me. I really struggle with them:
configuring development environments, fixing weird imports, etc. It's hard for
me, intellectually and emotionally.

I already did the parts that interested me the most and I want to move on to
something else.

Why ask for more funds then?

Well, lets say it plainly: the funds are not for me. I'm using the success I
had with the previous project and the interest NlNet has on it to fund other
people to finish the work.

Whoever that makes the task will get the budget associated to it.

The plan here is to help coordinate other people to make the tasks, but not
really do them myself. I don't discard it though. I'll probably need to work on
some of them.

The fact that I'm the one that presented the proposal doesn't mean the proposal
is for me. The proposal is for **you**.

### The people

I already managed to involve two fellow hackers and I made the proposal with
them as collaborators:

- **Efraim Flashner**, who has been working on the RISC-V port of most of the
  Guix packages is going to take part in this second stage of the project as he
  knows better than anyone else what's the status of RISC-V in Guix.
- **Danny Milosavljevic**, who worked in the bootstrapping process for ARM also
  agreed to get involved in this.
- **Jan Nieuwenhuizen** (Janneke) is the Mes author and maintainer.
- **Andrius Štikonas** is deeply involved in the bootstrapping process, too. Making
  a lot of patches to live-bootstrap, Mes, Hex0, M2-Planet and so on.
- **Juliana Sims** has also shown interest in the project because she has been
  involved in RISC-V related projects before.

You can also collaborate with us, if your contributions are good we can even
add you to the official team.

If you want to join us, feel free to contact me to `riscv-effort@elenq.tech` or
join `#bootstrappable` in `libera.chat` and ping me there.

I'm sure we all will learn a lot together during the process.


### Closing words

So, in summary, I'm just introducing a new part of this adventure. Thanks to
NlNet, we can take all the work we have been doing separately on Mes, GCC,
TinyCC, Hex0, M2-Planet and so on and finally combine it all together.

This is a huge effort, but hopefully we'll manage to do it, learn a lot in
the process and get paid.

We'll see how it goes. I'll keep you all informed.

Take care.
