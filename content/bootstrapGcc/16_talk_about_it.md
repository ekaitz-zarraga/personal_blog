Title: [Talk] Full Source Bootstrapping RISC-V on Guix
Date: 2024-09-19
Category:
Tags: Bootstrapping GCC in RISC-V
Slug: bootstrapGcc16
Lang: en
Summary: People from **Guix London** gave me the chance to describe this
    process.

Guix London[^london] gave me the chance to talk about this whole bootstrapping
process and I delivered.

- [Slides]({attach}/bootstrapGcc/guix-social.pdf)
- [Video recording (youtube)](https://www.youtube.com/watch?v=Cj7DyiRqWBk)

In the talk I explain what RISC-V is, how is the Linux support right now, and
which changes we added to `(gnu packages commencement)` in Guix.

Later, I share my thoughts about some social/emotional part, as I always do, in
this case focusing the attention in *leadership* and how our *sensei* **Ludovic
Courtès** is a great example of what I want to be as a free software developer
that was given some responsibility.

Then there are some questions, really good questions (thanks for that!), and we
discuss a little bit about **NlNet** and how the grants work. You might be
interested on that if you want to propose a project.

Just that, I hope you like the talk.

Fill my inbox with your questions, specially if they are interesting.


[^london]: Thanks to the people from Guix London: **Steve**, **Fabio** and
    **Arun**.
