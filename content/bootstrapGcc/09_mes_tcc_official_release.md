Title: Mes released and bootstrappable TCC merged
Date: 2023-11-16
Category:
Tags: Bootstrapping GCC in RISC-V
Slug: bootstrapGcc9
Lang: en
Summary:
    Some merging and releasing has been done. So here we are.

So, some merging and releasing has been done so we need to update a little bit
on what we talked about in the previous blog post.

### Mes

We spent some time more testing what we shared in the previous post with you
and now we can proudly say our work has been merged in Mes, and has been
released with it in GNU Mes 0.25.

You can read the GNU Mes 0.25 release notes in Janneke's blog in the following
link:

<http://joyofsource.com/gnu-mes-025-released.html>

### Bootstrappable TinyCC

We are also very happy to announce that our changes to the bootstrappable
TinyCC have been merged to Janneke's repository that is used for the official
Guix bootstrapping process. You can see the changes[^changes] being included here:

<https://gitlab.com/janneke/tinycc/-/tree/mes-0.25?ref_type=heads>

[^changes]: The commits we had have been reordered and squashed as the changes
    were split in around 40 different commits that were done as we found the
    errors. I managed to rearrange them in a few commits that have way more
    sense. I say it just in case you start looking for the independent commits:
    they are gone. My repository is still keeping the branches and tags we
    mentioned before so you can still go there to find the changes the way we
    did them.

### Some words about it

All of us are of course very happy about this, and this didn't make us relax,
as we continued to push fixes and test all this in more ways, looking always
for the next challenge.

We should enjoy this moment a little bit more, and that's why I am posting
this.

I want also to thank again the people that took part in this, specially Andrius
for his help, for all the hours of sleep he lost during the process and for
giving a second life to this effort, when I thought I was too tired to
continue; and Janneke, who very patiently reviewed every single contribution
and has been pushing me since the very early beginning of this adventure, when
I was thinking about accepting the challenge or continue with my life. I'm glad
I chose the adventure.

Of course, this is a cool milestone for all of us, we worked hard to make this
but specially for me it means a lot. I've been working on this for almost two
years now, and since I my changes on the bootstrappable TinyCC have been
sitting in my repository since the last year, when I finished the previous
NlNet grant. In fact, all that I did in that grant was sitting there, nothing
was merged in the actual Guix bootstrap, as what I did were very specific parts
of the chain, but they lacked the connection with other steps.

When you work in a project like that there's almost no satisfaction. No
releases, no upstreaming and, in my case, almost no help and no company.
Everything that I did could be sitting there in my repos forever.

At the time when I did the backport of TinyCC I was unsure of what I did and I
was exhausted after all the work I did with GCC. When we started this second
round I thought everything was going to be broken. And it was, but it was much
better than I thought!

Now, being part of the official Bootstrappable TinyCC means I can finally close
that chapter, which was full of uncertainty, and actually have some interesting
feedback of all that work I did that it seemed useless at the time when I did
it.

It happened to be useful after all.

Now let's see if GCC happens to be as useful as this was.

Cheers, dear reader. We deserve to celebrate.
