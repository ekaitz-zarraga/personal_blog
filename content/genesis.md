Title: Genesis
Date: 2018-04-15
Category:
Tags:
Slug: Genesis
Lang: en
Summary:
    About this blog, ElenQ Technology, and myself


As a first post in this *official-but-not-very-official* blog I just want to
introduce myself and *ElenQ Technology*.

First of all, my name is Ekaitz Zárraga and I was born in 1991. I describe my
job as R&D Engineer but actually I studied Telecommunications Engineering and I
*only* make my research and development in that area. I'm mostly focused in
programming or computer-related activities but I can also do some electronics
and other kind of things. That's my formal introduction. In the informal part
I'd say I've always been a really curious person and that made me try other
disciplines like arts in its different forms. This last point drives most of
what I'll write about later in this text. That's all from my part, I'll write
down an informal resume in the future.


*ElenQ Technology* is a name, it's a name to call the way I am and the
interests I have. That said, it's also the independent R&D project I'm running.
It's a different kind of company which aims to raise awareness about ethical
technology or [Ethical Innovation][1] by example, demonstrating ethical
companies can be profitable. It's not simply the way I make my living where I
make engineering, it's also a performance. Like an art piece.

> *ElenQ Technology* is an art piece which tells you that a different model is
> possible. It tells you that you have a choice and you don't need to work in a
> corporation and be governed by its rules.

*ElenQ Technology* is the result of many things I felt working for other
companies and it's also the result of a deep analysis of the status of the
technology in my context, which I think it can be generalized globally with a
decent accuracy.

First, in my near context most of the IT companies have a similar business
model based on *body shopping* and they pay really low salaries. Other sectors
are not in a much better position, but the IT is outrageous.

The jobs are not 9-to-5 jobs here. Working 10 hours per day is becoming the
norm.  The famous *Economical Crisis™* mixed with a deep corruption made people
pray for jobs and the companies are being aware of that.

That said, you can easily imagine how the tech world works here. IT
corporations get a ridiculous amount of money while they don't respect their
workers nor their clients. They make proprietary solutions because they don't
want to lose the projects and let the client be independent. They don't want
you to be free in any case because they need to maintain their rotten business
model.

That is the general status of the IT world in my surroundings but I'm sure, as
I said, it can be generalized, maybe not totally but there are many points than
can be, mostly because the corporations I mention are present in other
countries.

Personally, I had the luck to work in what I thought it was a better place.
The working conditions were not as bad as I described, or at least I
thought that. It was a R&D Engineer position in a not very big corporation. I
worked in a small department with less than ten co-workers. We made new stuff
for the company. It was fun.

After some time there I realized how it worked. It wasn't really different to
other jobs. There were a lot of things I don't want to share here but I started
to feel bad there and my personal situation didn't help at all. I've always
been a really curious person, I love learning new things and the job simply
wasn't giving me that as it did at the beginning. I started to need to fill my
needs spending more time after the job doing tech related stuff in the few free
time I had. The mix between the boring job and the organizational problems we
had made it really depressing.

While I was immerse in that depressing environment, our company wanted more
money and they started looking for new businesses with the resources they had.
Our team, as R&D team, was responsible of the development of the first
*proof-of-concept* of the new technology. We were asked to analyze the data
that the company had. Literally, we were asked to track people. The company
didn't care if they were our users or not, we were asked to track *everyone*.

That was the straw that broke the camel's back. I left the job because my
ethics are not compatible with people tracking. I don't like it and I don't
want to be part of it.

I always wanted to change the way the technology is created and I always
thought it was a great idea to make it by myself and encourage others to do so,
but I never had the courage to do it. What happened gave me the courage I
needed.

But, why not simply move to another job?

I think it was the moment to try it. I had been defining an idea of what
Ethical Technology is for a while and I always wanted to apply that idea in my
field: R&D. Also, taking in account that most of the companies where I could
work have the same structural problems I decided to change it from the root. I
decided to try a different model. Did I really have other options?

Is living in a depressing environment making the world a worse place to live
in an option? Really?

Think about it.


Now, *ElenQ Technology* breaks the business model of the companies of my
environment. It makes ethical technology and it makes it in an ethical way.
That's good for me because it lets me work on the fields I like and I can make
the world a better place to live in, which selfishly will improve the future I
leave for my children if I ever have them. But it's also good for the clients
*ElenQ Technology* has, because all the projects are handled in a way *they*
own them, following the principles of the Free Software and Hardware and
[Ethical Design manifesto](https://2017.ind.ie/ethical-design/) with some
extra ideas I added to be more specific to the innovation field (you can read
more [here][1]).

I want to change the world. Don't you?

The thing is I can't do it alone so *ElenQ Technology* wants to push other
people to take the same decision I took (or I was forced to take) and that's
its main goal.

Let's change the world together.

### About this website

You already noticed this is not the official *ElenQ Technology* website. This
is *my* *official-but-not-very-official* blog as part (or head, but I don't
like that word) of *ElenQ Technology*.

Here I'll write about *ElenQ Technology*'s philosophy, goals, achievements and
that kind of *official* things but mostly I'll write **about the things we
make**. That's what interests me the most.

This is going to be a really technical place where I'll try to explain advanced
concepts in a simple way to let you learn stuff with me. I want to share what I
do with you all.

### About languages

I'll write all this post in English but some of the entries are going to be
translated. As the blog supports that, it's better for me to leave it like a
supported tool and keep the translation option always enabled so I can add
community translated posts or posts translated by myself.

[1]: https://elenq.tech/en/about.html#ethical-innovation
