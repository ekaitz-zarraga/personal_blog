Title: My first time
Date: 2018-06-23
Category:
Tags:
Slug: First-Time
Lang: en
Summary: Thoughts about my first contribution to free software

The other day I remembered a very important day on my life, one of those early
beginnings that started to change my mind: **The first time I contributed to
free software**.

My first contribution was in 2014, more specifically the 22nd of May of 2014.

That's only 4 years ago. But, at the same time, they already passed 4 years
since then? OMG.

You get the feeling, right?

You may think I started coding when I was 10 or something like that. I didn't.
I learned programming in the university and not as well as a Computer Scientist
because I studied Telecommunication Engineering and computers are just a third
of the studies while the other two parts are electronics and signals related
things.

I'm not a young hacker or a genius. My parents don't like computers. I didn't
live with a computer at home since I was a toddler. That didn't happen.

Today I want to tell you my story. Not because it's awesome and you'll love it.
I want to tell you my story because it's **really** standard. I want you to see
that you can also contribute to Free Software. Anyone can.

So, how did it all start?

I started my university studies in 2009. The first year we had one semester of
C and the next one of C++. Not real programming classes, just introductory
stuff for the languages and computers. A couple of years later we had a
networking subject where I used Linux for the first time. The computers had
*Kubuntu* installed. At that time my laptop started to give me some trouble and
I installed *Kubuntu* in a dual boot and tested it. It was nice.

Few time later the *Windows* partition failed again and I was comfortable
enough in *Kubuntu* to delete it and use only *Kubuntu*. It was easy.

The second semester that year another subject had some focus on Linux because
it was a networks and tools subject and I really needed it. We learned to use a
terminal, some SQL and many things like that. Simple tools but they resulted to
be useful in the future. I was really surprised by the power of the terminal
and I studied a lot in my free time I finished the subject with honours just
because I was really interested on it. As I said, I'm not a genius, I was
interested.

We had a subject about *Minix*, following Andrew Tannenbaum's *Operating
Systems: Desing and Implementation* book and *Minix* version 1, which gave us
the initial needed knowledge about Operating Systems at that time. That started
to give me some info about the ethical part of the free software and also
sparked more interest.

Next year I had a couple of Operating Systems subjects (the theoretical one and
the practical one). The teacher was part of *KDE Spain*, and he talked about
free software in class. I was quite into it at that time. The practical part of
the subject was real software, we covered the contents of the book called
*Advanced Linux programming*[^1]. That was pure C development and we didn't
have a lot of knowledge on that. We just touched some C/C++ during the first
year and some assembly in a couple of subjects. It was really hard, but it was
really cool.

We made a small shell. It was great!

Final year[^2] of the university: I had to make the final project.

I didn't know what to do so I contacted the teacher who was part of *KDE Spain*
and he mentored me. I installed a IRC client and started talking with the
people at *kde-telepathy* project. I wasn't used to that kind of collaborative
development. Heck, I wasn't used at any kind of development! But it was all
good, mostly thanks to the great people in the project (David, Diane, George,
Martin... *You* are awesome!).

The project itself was a *KDE* application, *KDE-Telepathy*, a big one. Thanks
to heaven, my part of the project was quite separated so I could focus on my
piece. That taught me to search in a big codebase and focus on my part. Then I
had to code in C++ like in the real life, not like designed problems I've
worked on at the university, and I also had to read tons of documentation about
*Qt*, *KDE* and anything else.

I started with the contribution that opened this post and I went on until I
renewed the whole interface. It wasn't great, but the code was finally merged
in the application some time later.

Since then I could say I code almost everyday and I've been studying many
languages more but, at that time, I was relatively new to programming and
computers.

With all this I mean:

> If you are interested, try. Everything is going to be fine. You don't need to
> be a genius[^3].

[You can check the contribution
here](https://git.reviewboard.kde.org/r/118256/diff/2#index_header).

Love.

Ekaitz

[^1]: It's a great book, by the way. You can find it
  [online](https://mentorembedded.github.io/advancedlinuxprogramming/).

[^2]: When I studied, right before the [Bolognia
  Process](https://en.wikipedia.org/wiki/Bologna_Process), the university was 5
  years long for a Masters Degree and 3 for a Bachelor Degree.

[^3]: But congratulations if you are, that way you'll learn faster and probably
  have more reach if you want to.
