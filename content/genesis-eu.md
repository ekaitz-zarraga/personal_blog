Title: [EU] Genesis
Date: 2018-04-15
Category:
Tags:
Slug: Genesis
Lang: eu
Translation: true


Blog *ofizial-baina-ez-oso-ofizial* honen lehenengo post bezala, nor naizen eta
*ElenQ Technology* zer den aurkeztu nahi dut.

Hasteko, Ekaitz Zárraga naiz eta 1991. urtean jaio nintzen. Nire burua I+G
ingeniari bezala deskribatzen dudan arren, Telekomunikazio Ingeniaritza ikasi
nuen eta arlo horretan *bakarrik* egiten dut lan. Bereziki ordenagailuekin
lotutako gauzak egiten ditut, programazioa eta horrelakoak, baina elektronika
eta bestelako gauzak jorratzeko ere gaitasuna daukat. Hori da nire sarrera
formala. Sarrera informalean jakin-min handia dudala esango nuke eta horrek
beste diziplina batzutan aritzeko aukera eman didala, haien artean artea.
Azkeneko puntu honek garrantzi handia dauka testu honetan idatzitakoarekin
erlazio zuzena izango duelako. Hauxe da nire aurkezpena, etorkizunean kurrikulum
informal bat egingo dut.

*ElenQ Technology* izen bat da, nire interesak eta nire izaera adierazteko izen
bat baino ez. Hori esanda, aldi beran nire I+G proiektu independentea da.
Adibidearen bitartez, enpresa etikoak errentagarriak izan daitezkeela
erakutsiz, teknologia etikoari buruz kontzientzia eratzeko helburua duen
enpresa bat da. Ez da nire lana bakarrik, *performance* artistiko bat da.
Artelan baten antzera.

> *ElenQ Technology* modelo ezberdin bat egin daitekeela esaten dizun artelan
> bat da. Aukerak badaudela eta ez zaudela korporazio baten arauekin lan
> egitera behartuta esaten dizu.

*ElenQ Technology* beste enpresetan lan egitearen eta nire ingurunean
teknologiaren egoeraren analisi bat egitearen emaitza da, baina uste dut nahiko
modu zehatzean orokortu daitekeela.

Hasteko, nire inguruko IT enpresek *body-shopping*-ean oinarritutako negozio
eredu antzekoa daukate. Askotan ilegalak diren praktikak egiteaz gain soldata
baxuak ordaintzen dituzte. Beste sektoreak ez dira askoz hobeak baina IT
munduaren kasua guztiz ankerra da.

Ordutegiak luzatu egin dira azken urteotan. Egunean 10 ordu lan egitea normala
bihurtzen hasi da. *Krisi Ekonomiko™* famatuaren ondorioz, prekaritatea
normalizatu egin da, gehien bat, enpresek argi daukatelako jendeak lanaren
behar larria daukala.

Hori esanda, erraz ulertu dezakezu teknologiaren mundua nola dagoen. IT
korporazioek haien bezeroak eta langileak errespetatu gabe dirutza egiten dute.
Bezeroak lotuta izateko produktu propietarioak saltzen dituzte. Haien helburu
nagusia daukaten negozio eredu ustelaren etorkizuna bermatzea da.

Hau da bizi dugun egoera. Lehen esan bezala, erraz orokortu daiteke, aipatutako
enpresa gehienak atzerrikoak direlako eta beste herrialdeetan ere kokatuta
daudelako.

Nire kasuan, leku hobe batean lan egiten nuela uste nuen. Lan baldintzak
hobeak ziren bertan. I+G ingeniari postu bat nuen enpresa moderno batean.
Hamar bat pertsonaz osatutako departamendu txiki isolatu bat zen. Enpresaren
jostailu berriak egiten genituen. Nahiko dibertigarria zen.

Denbora pasa ahala, lan baldintzak hain onak ez zirela konturatzen hasi
nintzen. Aipatu nahi ez ditudan gauza asko gertatu zirenez, bertan txarto
sentitzen hasi nintzen eta, gainera, nire egoera pertsonalak ez zuen batere
lagundu. Oso pertsona kuriosoa naiz eta lanak nire jakin-mina asetzeko ematen
zidan aukera desagertzen hasi zen. Nire denbora librean gauza berriak ikasten
eta aztertzen hasi nintzen. Lan aspergarria barneko antolaketa arazoekin batu
zen eta nire bizitza kudeatzea oso zaila egin zitzaidan.

Depresioan murgilduta, enpresak, burtsara ateratzea helburu zuela, bere
negozioa handitu nahi zuen. Gure departamenduak, enpresaren I+G-aren
erantzulea zenez, enpresaren kontzeptu-proba berrien garapena egin behar
zuen. Enpresak zituen datuak aztertzea eskatu ziguten. Pertsonen kokapena
jarraitzea eskatu ziguten, mundu osotik, edozein momentuan. Ez zuten bezero eta
ez-bezeroen arteko ezberdintasunik egin nahi. *Guztiak* jarraitzeko eskatu
ziguten.

Hori gehiegi zen niretzat. Arrazoi etikoengatik utzi nuen lana. Ez dut horretan
parte hartu nahi.

Teknologia eratzen den modua aldatu nahi izan dut beti. Nire kabuz teknologia
egitea eta besteak gauza bera egitera bultzatzea beti egon da nire buruan baina
orain arte ez dut salto hori emateko ausardirik izan. Gertatutakoak falta
zitzaidan bultzada eman zidan.

Baina, zergatik ez mugitu beste lan bateara?

Momentua zela uste dut. Denbora luzez ibili naiz teknologia etikoari buruz
pentsatzen eta nire esparruan, Ikerkuntza eta Garapenean, aplikatu nahi nuen.
Gainera, nire inguruko enpresetan izango nituen arazoak ikusita, zuzenean
sustraira joatea erabaki nuen. Beste modelo bat saiatzea erabaki nuen. Beste
aukerarik al nuen?

Mundua txarrerantz aldatzen giro deprimagarri batean lan egitea benetako aukera
bat al da?

Pentsatu ondo.

Orduan, *ElenQ Technolgy*-k nire inguruko konpainien negozio modeloa apurtzen
du. Teknologia Etikoa garatzen du, modu etiko batean. Hori ona da niretzat,
zuzenean, niri gustatzen zaizkidan gauzetan lan egiteko aukera ematen didalako,
etorkizunean eduki ditzakedan umeentzat mundua hobetzen dudan bitartean. Eta
bezeroentzat, proiektuak garatutako teknologia *bezeroarena* izateko moduan
kudeatzen ditugulako, Software eta Hardware Librearean printzipioak eta
[Diseinu Etikoaren Manifestoa](https://2017.ind.ie/ethical-design/) (gure
esparrura moldatuta) jarraituz (gehiago irakurri dezakezu [hemen][1]).

Mundua aldatu nahi dut, zuk ez?

Nik bakarrik ezin dudala mundua aldatu konturatu naiz, beraz, *ElenQ
Technology*-k besteak nik (behartuta edo ez) hartu nuen erabakia hartzera
bultzatzea du helburu.

Aldatu dezagun mundua guztiok batera.

### Blog honi buruz

Jada konturatu zara blog hau *ElenQ Technology*-ren blog ofiziala ez dela.
*Nire* blog *ofizial-baina-ez-oso-ofiziala* da, *ElenQ Teknology*-ren parte
(edo buru, baina ez dut hitz hori gustoko) bezala.

Hemen *ElenQ Technology*-ri buruz idatziko dut, bere filosofia, helburu,
arrakasta, etab.-ei buruz. Baina gehien bat **egiten dugunari buruz** idatzi
nahi dut, hori baita niretzat interesgarriena. Prozesuaren parte egin nahi
zaituztet.

### Hizkuntzei buruz

Blog hau ingelesez idatziko da, baina aukera dago testuak (hau bezala) beste
hizkuntzetara itzultzeko. Blogak baimentzen duen bitartean nahiago dut aukera
prest uztea komunitateak itzulitako testuak gehitzeko edota, kasu honen moduan,
nik egindako itzulpenak igo ahal izateko.

[1]: https://elenq.tech/eu/about.html#ethical-innovation
