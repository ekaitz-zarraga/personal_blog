Title: ElenQ Donations — Intro + GNU Guix
Date: 2020-05-25
Categories: Series
Tags: ElenQ Donations
Slug: donations-guix-01
Lang: en
Summary: Recent ElenQ Technology donation to the great GNU Guix package manager
    and software distribution

I consider my work part of my responsibility to make this world a better place
so since the early beginning of the company I decided to donate as much as I
could to the free software projects I was using for my work in order to help
the ecosystem be sustainable.

Many times, free software projects that are being extensively used by companies
are considered just *free* products that don't carry any kind of responsibility
with them. It is fine to use free software for your own goals (that's the
freedom 0), but it's not morally acceptable to base your business model on a
project that independent developers made with no funds (or with very low funds)
and don't even consider helping them.

We already [had cases][gpg] of free software developers that are keeping the
projects running with their own expenses while the whole <del>fucking</del>
world is just *using* the software they make without thinking about their
conditions.

ElenQ Technology has been founded by a not-very common individual. That's
obvious.

Sadly, sometimes ElenQ Technology simply can't afford to donate a part of our
income[^1]. But I can code.

I can always share my time between projects and my free time on trying to
support free software projects that make <del>my</del> our life easier.

### GNU Guix

[GNU Guix](https://guix.gnu.org) is one of those projects. I started using it a
couple of months ago as a package manager and now I moved to the full software
distribution.

For those who don't know Guix yet, it's a package manager and a software
distribution like Nix and NixOS are. They are based on the same principle and
have the same core.

The innovation they carry is the transactional package manager that eases
rollbacks and isolated environment creation. In the case of the software
distribution, the whole system can be described by an easy-to-write file that
is also version controlled, so you can always recover an older configuration if
you need to.

All the packages and system descriptions are defined in code. In Nix, they are
defined with Nix programming language (a DSL for that purpose). In Guix, they
use Guile (scheme) programming language (a general-purpose programming
language).

As my work at ElenQ forces me to visit many different codebases and use a wide
variety of software in short term projects, Guix is very handy for me. I can
create a new isolated environment, code on it and, once I'm done, remove it
from my system in the cleanest way.

Also, package definition is easy and straight to the point, so I can package
anything I want just coding few lines.

It's an interesting project for system administrators too. Machines are easy to
replicate with it and it's easy to go back if you screwed up in the
configuration.

Further than that, they are working really hard on reproducible builds and the
chain of trust that modern software needs.

You should check the project yourselves better for more detailed info.

### So what?

Since I use the project I'm started to take part on it, packaging new code and
sending simple patches. More I get involved on it more I will do. I'm not
really used to Guix yet, so I didn't dig the code deeply enough and I'm not
able to code very complex stuff on it.

At the moment, I'm trying to package **Meshlab**, a 3D mesh editing software
you've probably heard about.

For that I packaged (already merged) `openctm`, `lib3ds` and `openkinect` (in
its three flavors: C/C++ lib, python bindings and open-cv bindings). And during
that time I also found a couple of details I could improve and I made some
patches for them too.

In the past I also contributed with few package patches, on `chicken` and
`chibi` scheme implementations and `kitty` terminal emulator. You can find all
of them searching my name in the issue board you'll find in the following link:

<https://issues.guix.gnu.org>

GNU Guix is not a very big project and it doesn't have a large userbase that
can help them grow fast and reliably so it needs some extra help, from me and
surely from you. They have been a very welcoming community so I encourage you
to take part if you are interested on it.

I hope this helps to spark your interest on helping on any project you like and
maybe pressure your company to spend some resources on helping any project they
use.


Stay safe.

[gpg]: https://www.propublica.org/article/the-worlds-email-encryption-software-relies-on-one-guy-who-is-going-broke

[^1]: You can change that [hiring us](mailto:hello@elenq.tech).
