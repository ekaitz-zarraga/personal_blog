Title: ElenQ Donations — Chibi Scheme
Date: 2020-05-31
Categories: Series
Tags: ElenQ Donations
Slug: donations-chibi-02
Lang: en
Summary: Donation to Chibi Scheme programming language

In a previous post I already talked about why I consider important to donate
money or time to Free Software projects.

This time I want to talk about my recent contributions to Chibi Scheme's
standard library.

Chibi Scheme is a R7RS scheme implementation that was designed to work as an
extension or scripting language. It's just a small library you can embed.
That's similar to Lua, but with *a lot* of parenthesis[^1].

For those that are not familiar with Scheme: it's just a programming language
you should read about. You'll probably discover all those new cool things you
have in your programming language of choice are not that new[^2].

There's a detail I'd like to talk about, though. Contrary to other programming
language definitions or standards, Scheme's R7RS report is something you can
read yourself. It's less than 100 pages long[^3].

If you combine that with the design decisions that Alex Shinn (who also took
part on the R7RS definition) took on Chibi Scheme, you end up with a *simple*
programming language you can actually read.

That's important.

You might wonder why should you care about the readability of a programming
language if you are just a user. The answer is simple too: free software relies
in the fact that you can audit and improve or customize it. If you are not able
to read it you can't exercise your rights by yourself and you'll always need to
rely in someone else. That's not intrinsically bad, that's the only solution
that non-programmer users have. Programmers need to trust other people in other
things as well so that's not a major issue.

Problems come when projects get so complicated —and I mean
millions-of-lines-of-code complicated here— only large companies have enough
resources to tackle the task of editing the code. In those cases, software is
not really free anymore, because *in practice* you are unable to use your
rights and you can't afford to find someone else to do it for you.

We started to get used to that, though.

Something I learned as a sculptor is the tools that fit you better are those
that you made, you hacked or you get used to. As programmers, we are supposed
to know how to program, so we are supposed to be able to make and hack the
tools but we are deciding to get used to the ones that others built.

The first step to control your workspace, and consequently your own job is to
control your tools[^4].

I'd love to say those are the reasons why I use Chibi Scheme, but that's not
*totally* true. I don't know why I use it. I just *like it*.

Anyway, the other day I realized Chibi Scheme's JSON parser was unable to parse
Spanish accents so I was unable to control [ElenQ
Publishing's](http://en.goteo.org/project/elenq-publishing) book's metadata
correctly. That's a problem.

As the language is simple, I was able to read the standard library and propose
a change that would let the JSON parser use UTF-8 characters.

<https://github.com/ashinn/chibi-scheme/pull/643>

During the process I checked [CPython's JSON parser
implementation](https://github.com/python/cpython/blob/master/Lib/json/decoder.py#L117)
and I realized I could do it better adding surrogate pair support. So I decided
to add it too.

<https://github.com/ashinn/chibi-scheme/pull/644>

Once my changes were merged, I realized it was a good idea to keep going and
add a JSON encoder, that wasn't developed yet. So I did.

<https://github.com/ashinn/chibi-scheme/pull/648>

While I was testing my JSON encoder I realized there was an issue with floating
point numbers in the JSON parser. So I fixed that too.

<https://github.com/ashinn/chibi-scheme/pull/647>

I also fixed some random indentation issue I found:

<https://github.com/ashinn/chibi-scheme/pull/646>

I didn't really need to do all what I did, but I did it anyway. I just wanted
to keep Chibi Scheme healthy while I was opening the door to some future
contributions. Now I have a little bit more control on my tooling, and I feel
more comfortable with the fact that I might need to make some changes on
Chibi's code in the future.

It doesn't need to be perfect, neither. I'm sure it isn't, because I didn't
write C code since I was at university and I had zero experience working on
Chibi-Scheme's internals. My code was just enough to make the features happen,
now with Alex's changes the code is running fine and **everyone** can benefit
from this.

So, the message I take from this can be summarized in these points:

- Use tools you can read and edit like Chibi Scheme or even CPython, which is a
  large codebase but is surprisingly easy to read.
- Programming languages (or their stdlib) should never be considered something
  untouchable. Touch them, change them, make them fit your needs.
- Don't be afraid of tackling something that may seem hard on the first look.
- You don't have to be perfect.
- Spend time and energy on stuff that matters.

Hope all this —the post and the code— is useful.

Being useful is the greatest goal of an engineer, after all.

Take care.

[^1]: And no `end`-s. Less typing for sure: good for your hands.

[^2]: And maybe not that cool neither.

[^3]: I'm not going to talk about the implications of that fact. It's obvious
  there must be some kind of trade-off comparing to other standards that are
  more than one thousand pages long. I'll just recommend you to read it, it's
  pretty good: <https://small.r7rs.org/attachment/r7rs.pdf>

[^4]: In fact, that's the second. I'm supposing we all know what we are doing.
