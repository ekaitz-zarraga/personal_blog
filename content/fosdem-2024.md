Title: FOSDEM and Guix Days 2024
Date:  2024-02-12
Category:
Tags:
Slug: fosdem-2024
Lang: en
Summary: About my personal FOSDEM 2024 experience and Guix Days

This year I gave a talk at FOSDEM, summarizing our work on the Guix
bootstrapping for RISC-V, so I decided to get a couple of free days more and
also visit the Guix Days, that where happening before the FOSDEM itself. Let's
do a short summary of my experience here.

### Guix Days

I visited the Guix Days but not full-time because I wanted to spend some time
in Brussels itself, rather than being locked in a place for the whole day.

We had some interesting discussions about Guix, the most interesting for me was
the Guix Governance, where we discussed how Guix is managed and how it works
internally in a social level. This discussion was specially important for me as
an external contributor because I believe Guix has a complex but opaque
internal structure that is difficult to grasp from the outside. Being in places
like the Guix Days themselves let's you understand it but it's our
responsibility to make Guix accessible for people that doesn't have time to
come to these kind of events.

I say all this because I'm basically that person. I don't enjoy this kind of
events that much and I feel I was a little bit forced to be there to be a
little bit more than a random string in the IRC chat.

I had the chance to be there, but it was an effort to me. I'd like it not to be
the same for others.

It's not like I'm an antisocial, in fact I feel I'm a very social person, but I
don't like the politics of things and this event, more than anything else, I
felt it was a political event where I had to be just to show up.

This is not just a Guix problem. Most large enough project fall into this kind
of dynamic, where people that show up are better considered that those who
don't. It makes sense (this is how the world works), but at the same time it
doesn't (I don't like how the world works).



### FOSDEM 2024

We arrived FOSDEM on Saturday. It was literally impossible to do anything
there. It was basically supercrowded. We tried to watch some talks, all were
full. Waited for a long queue to enter one and we didn't have the chance to
enter in the end and we decided to leave. We had a nice day in the city instead.

[Sunday morning I gave a talk][talk]. Sunday was way better: we could walk
around and do things but we spent the morning in the Declarative and
Minimalistic Computing Devroom until my talk happened. After my talk, we
watched a couple more there and left for a very good lunch.

I think the talk went well, but I'll leave further judgement to you. Feel free
to watch and send me feedback if you like to do so.

[talk]: https://fosdem.org/2024/schedule/event/fosdem-2024-1755-risc-v-bootstrapping-in-guix-and-live-bootstrap/



### My feelings

In a personal level, traveling (taking flights and all that...) is mentally
exhausting for me, and it's also expensive. I don't feel like I would do this
often in the future, as I didn't do it in the past.

Also, I don't enjoy geeky events like these that much. I don't use Guix or any
other software as a part of a tribe, I just think it's useful. I enjoy other
kind of social interactions more. I just felt like an outsider in both events,
but I don't really want to become anything else than that. I don't feel
comfortable with becoming "part" of anything. I believe software is not a cult,
and everyone should have the freedom to contribute and enjoy in a purely
practical way, with no identities involved.

Also I felt like people around those higher latitudes are colder, they laugh
less than I do and they don't have the boiling blood that I have. Maybe that's
why my talk made people laugh and react. There's nothing wrong about that,
culture is always cool, but the culture mismatch I felt it was a little bit of
a barrier.

Apart form all that, I had the chance to visit a cool city with my significant
other and with my friends, who came to support me in my talk and enjoy a
conference. We probably didn't enjoy the conference that much, but I
experienced being surrounded by people that love me and had a lot of fun with
them, and that's more valuable than anything else.

On the other hand, I don't need FOSDEM for that. I feel grateful for my people
every single day of the year.

Don't expect to find me in many geek events like these, but I don't totally
discard showing up from time to time.
