Title: About

<style>
figure{
    margin: auto;
    width: 300px;
}
figure img{
    margin: 0;
    height: auto;
    width: 100%;
}
figure figcaption{
    padding: 1rem;
    font-size: 90%;
    font-style: italic;
    width: 100%;
}
</style>

This is Ekaitz Zárraga, a guy born in 1991[^1] who is based in Bilbao.

I studied Telecommunication Engineering (2009-2014)[^2] in the Faculty of
Engineering in Bilbao at the University of the Basque Country.

Since then I've been working for several companies until I [decided
to][genesis] found ElenQ Technology, what at the moment is a one-person company
where I work as a Research and Development engineer in my areas of knowledge.

This blog was born as a way to share what I do at my job at ElenQ Technology,
my thoughts and my decisions, but it's also a way to represent the way I work
and the things I like. Sometimes, I use this space to share stuff I learned
thanks to the R&D projects or as a backlog of what I do.

### Contact

You can find me in the fediverse at
[@ekaitz_zarraga@mastodon.social](https://mastodon.social/@ekaitz_zarraga) and
you can send me an email to [hello@elenq.tech](mailto:hello@elenq.tech).

<figure>
<img src="{static}/static/images/ekaitz.jpg">
<figcaption>Me in Astorga</figcaption>
</figure>

[genesis]: https://ekaitz.elenq.tech/Genesis.html

[^1]: Around the `663350400` Unix timestamp if you need a more reliable source.
[^2]: At that time Master's Degrees were 5 years long.
