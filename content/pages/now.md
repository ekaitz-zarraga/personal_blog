Title: Now
status: hidden

> First edition of this post is from 15-09-2020

I'm an Research and Development engineer but that doesn't describe my job very
well. What I'm doing now doesn't neither but it's a good way to show how random
my lifestyle is. All these points are the things I've been doing for the last
few months that are still active in my mind.

On the keyboard:

- I'm the guy behind ElenQ Technology, as you know.
- I'm interested on programming languages and the design decisions behind them.
    - I'm coding a lot of Scheme these days, mostly in Chibi, but also in Guile
      for my Guix.
    - I'm studying Common Lisp as a replacement for Clojure for some stuff
      where I don't want to install a JVM.
    - Still do a lot of Python.
    - I'm able to code in many other languages, though, but I may or may not be
      interested on starting my own projects on them.
- I'm making a publishing house to make technology accessible for everyone at:
  <https://en.goteo.org/project/elenq-publishing>
    - The publishing house forces me to write some of the contents, translate
      others and prepare all the paperwork.
    - It also made me prepare a full set of tools for preparing the files
      automatically that even create the covers of the books programatically.
- I teach advanced computing topics for companies or individuals who need them.
  I prepare the courses on demand and sometimes they let me write actual books
  as course material. Last months I taught these topics:
    - Advanced Python (20 hours)
    - Web Scraping (30 hours)
    - Modern C++ (30 hours)

Out of the keyboard:

- I'm interested on bookbinding. I made several notebooks and photo albums for
  my friends (I'll post pictures). 
- I learned about leather craft and made some wallets too.
- I'm making simple 2D plotters with scavenged electronics.
- I'm working on a simple computing device for coders and writers, but I didn't
  talk about it yet[^ssh].
- I spend a lot of time and resources trying to heal myself.

[^ssh]: Ssssh! Don't snitch!
